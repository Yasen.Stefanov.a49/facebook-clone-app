import { IsNotEmpty } from 'class-validator';

export class CreateImageDto {
  @IsNotEmpty()
  filename: string;

  @IsNotEmpty()
  mimetype: string;

  @IsNotEmpty()
  encoding: string;

  @IsNotEmpty()
  buffer: Buffer;
}
