import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ImageEntity } from '../image/entity/image.entity';
import { DeepPartial } from 'typeorm';
import { UserEntity } from 'src/user/entities/user.entity';

@Injectable()
export class ImageService {
  constructor(
    @InjectRepository(ImageEntity)
    private readonly imageRepository: Repository<ImageEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  async createImage(
    file: Express.Multer.File,
    userId: number,
  ): Promise<ImageEntity> {
    const { originalname, mimetype, buffer } = file;
    const user = await this.userRepository.findOne({ where: { id: userId } });
    const image: ImageEntity = this.imageRepository.create({
      filename: originalname,
      imageData: buffer,
      description: 'asd',
      mimetype,
      user: user,
    } as DeepPartial<ImageEntity>);

    await this.imageRepository.save(image);
    return image;
  }
  async getImageDataFromDatabase(id: number): Promise<ImageEntity | null> {
    const image = await this.imageRepository.findOne({ where: { id: id } });
    if (!image) {
      return null;
    }

    if (!image.imageData) {
      return null;
    }

    return image;
  }
  findAllImages = async (): Promise<ImageEntity[]> => {
    try {
      const images = await this.imageRepository.find();
      return images;
    } catch (error) {
      console.error('Error in findAllImages:', error);
      throw new Error('An error occurred while fetching images.');
    }
  };
  convertByteArrayToImage = (file): string | null => {
    try {
      const byteArray = new Uint8Array(file.imageData);
      const base64Image = Buffer.from(byteArray).toString('base64');
      const dataUrl = `data:image/jpeg;base64,${base64Image}`;
      return dataUrl;
    } catch (error) {
      return null;
    }
  };
}
