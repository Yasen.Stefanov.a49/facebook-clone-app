import {
  Controller,
  Post,
  Get,
  Param,
  UseInterceptors,
  UploadedFile,
  Inject,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ImageService } from './image.service';

@Controller('images')
export class ImageController {
  constructor(
    @Inject(ImageService)
    private readonly imageService: ImageService,
  ) {}

  @Get()
  findAll() {
    return this.imageService.findAllImages();
  }
  @Get(':id')
  getImage(@Param('id') id: number) {
    return this.imageService.getImageDataFromDatabase(id);
  }

  @Post(':userId')
  @UseInterceptors(FileInterceptor('file'))
  async uploadImage(
    @UploadedFile() file: Express.Multer.File,
    @Param('userId') userId: number,
  ) {
    try {
      const image = await this.imageService.createImage(file, userId);
      return image;
    } catch (error) {
      console.error('Error uploading image:', error.message);
      return { success: false, error: error.message };
    }
  }
}
