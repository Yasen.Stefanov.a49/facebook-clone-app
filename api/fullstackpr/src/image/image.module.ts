import { Module } from '@nestjs/common';
import { ImageEntity } from './entity/image.entity';
import { ImageService } from './image.service';
import { ImageController } from './image.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from 'src/user/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ImageEntity, UserEntity])],
  providers: [ImageService],
  controllers: [ImageController],
})
export class ImageModule {}
