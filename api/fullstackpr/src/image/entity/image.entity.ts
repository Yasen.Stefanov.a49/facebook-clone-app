import { UserEntity } from 'src/user/entities/user.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

@Entity()
export class ImageEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar' })
  filename: string;

  @Column({ type: 'text', default: '', nullable: true })
  description: string;

  @Column({ type: 'bytea' })
  imageData: Buffer;

  @Column()
  userId: number;

  @ManyToOne(() => UserEntity, (user) => user.images)
  user: UserEntity;
}
