import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from 'src/user/entities/user.entity';
import { PostService } from './post.service';
import { PostController } from './post.controller';
import { PostEntity } from './entity/post.entity';
import { UserService } from 'src/user/user.service';
import { ImageEntity } from 'src/image/entity/image.entity';
import { ImageService } from 'src/image/image.service';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, PostEntity, ImageEntity])],
  providers: [PostService, UserService, ImageService],
  controllers: [PostController],
  exports: [PostModule],
})
export class PostModule {}
