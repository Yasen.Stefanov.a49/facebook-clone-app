import {
  IsArray,
  IsInt,
  IsNotEmpty,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

class LikedByDto {
  @IsInt()
  @IsNotEmpty()
  userId: number;
}

export class CreatePostDto {
  @IsInt()
  @IsNotEmpty()
  from: number;

  @IsString()
  @IsNotEmpty()
  data: string;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => LikedByDto)
  likedBy: LikedByDto[];

  @IsArray()
  @IsString({ each: true })
  commentedBy: string[];

  @IsArray()
  @IsString({ each: true })
  sharedBy: string[];
}
