import { UserEntity } from 'src/user/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  ManyToMany,
  JoinTable,
} from 'typeorm';

@Entity()
export class PostEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'int' })
  userId: number;

  @Column({ type: 'varchar' })
  data: string;

  @Column({ type: 'varchar', array: true, default: [] })
  commentedBy: string[];

  @ManyToOne(() => UserEntity, (user) => user.posts)
  user: UserEntity;

  @ManyToMany(() => UserEntity, (user) => user.likedPosts)
  @JoinTable()
  likedBy: UserEntity[];

  @ManyToMany(() => UserEntity, (user) => user.sharedPosts)
  @JoinTable()
  sharedBy: UserEntity[];
}
