import {
  Controller,
  Post,
  Get,
  Param,
  Inject,
  Body,
  Patch,
} from '@nestjs/common';

import { PostService } from './post.service';

@Controller('posts')
export class PostController {
  constructor(
    @Inject(PostService)
    private readonly postService: PostService,
  ) {}

  @Get()
  findAll() {
    return this.postService.findAllPosts();
  }
  @Get(':id')
  getPost(@Param('id') id: number) {
    return this.postService.getPostFromDatabase(id);
  }

  @Patch(':postId')
  handlePostAction(
    @Param('postId') postId: number,
    @Body('userId') userId: number,
    @Body('action') action: string,
  ) {
    switch (action) {
      case 'like':
        return this.postService.likePost(userId, postId);
      case 'share':
        return this.postService.sharePost(userId, postId);
      default:
        throw new Error('Invalid action');
    }
  }

  @Post()
  async createPost(@Body() postData: { userId: number; data: string }) {
    try {
      const post = await this.postService.createPost(postData);
      return post;
    } catch (error) {
      console.error('Error creating Post:', error.message);
      return { success: false, error: error.message };
    }
  }
}
