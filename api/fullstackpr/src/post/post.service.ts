import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PostEntity } from './entity/post.entity';
import { UserService } from 'src/user/user.service';
import { UserEntity } from 'src/user/entities/user.entity';

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(PostEntity)
    private readonly postRepository: Repository<PostEntity>,
    private readonly userService: UserService,
  ) {}

  async createPost(postData: {
    userId: number;
    data: string;
  }): Promise<PostEntity> {
    try {
      const { userId, data } = postData;
      const post: PostEntity = this.postRepository.create({
        userId,
        data,
      } as Partial<PostEntity>);
      if (!userId || !data) {
        throw new Error('Invalid input data');
      }
      await this.postRepository.save(post);
      return post;
    } catch (error) {
      console.error('Error creating and saving post:', error.message);
      throw new Error('Failed to create and save post');
    }
  }

  async getPostFromDatabase(id: number): Promise<PostEntity | null> {
    const post = await this.postRepository.findOne({
      where: { id: id },
      relations: ['likedBy'],
    });
    if (!post) {
      console.error('post not found!');
      return null;
    }

    return post;
  }
  async likePost(userId: number, postId: number): Promise<PostEntity> {
    const post = await this.postRepository.findOne({
      where: { id: postId },
      relations: ['likedBy'],
    });
    if (!post) {
      throw new Error('Post not found');
    }

    if (!post.likedBy.some((user) => user.id === userId)) {
      const user = await this.userService.viewUser(userId);

      if (!user) {
        throw new Error('User not found');
      }
      post.likedBy.push(user);
      await this.postRepository.save(post);
    }

    return post;
  }

  async sharePost(userId: number, postId: number): Promise<PostEntity> {
    const post = await this.postRepository.findOne({
      where: { id: postId },
      relations: ['sharedBy'],
    });
    console.log(post);
    if (!post) {
      throw new Error('Post not found');
    }

    if (!post.sharedBy.some((user) => user.id === userId)) {
      const user = await this.userService.viewUser(userId);

      if (!user) {
        throw new Error('User not found');
      }
      post.sharedBy.push(user);
      await this.postRepository.save(post);
    }

    return post;
  }

  async findAllPosts(): Promise<any[] | null> {
    try {
      const posts = await this.postRepository.find({
        relations: ['likedBy', 'sharedBy'],
      });
      if (posts.length === 0) {
        return null;
      } else {
        const usersArray = await Promise.all(
          posts.map(async (post) => {
            try {
              const user = await this.userService.viewUser(post.userId);

              return {
                ...user,
                data: post.data,
                postId: post.id,
                likedBy: post.likedBy,
                sharedBy: post.sharedBy,
              };
            } catch (error) {
              console.error('Error in processing post:', error);
              return null;
            }
          }),
        );

        const convertedData = await this.userService.convertUserData(
          usersArray.filter((user) => user !== null) as UserEntity[],
        );

        return convertedData;
      }
    } catch (error) {
      console.error('Error in findAllPosts', error);
      return null;
    }
  }
}
