import { Controller, Post, Get, Param, Inject, Body } from '@nestjs/common';

import { NotificationService } from '../notifications/notification.service';

@Controller('notifications')
export class NotificationsController {
  constructor(
    @Inject(NotificationService)
    private readonly notificationService: NotificationService,
  ) {}

  @Get()
  findAll() {
    return this.notificationService.findAllNotifications();
  }
  @Get(':id')
  getNotification(@Param('id') id: number) {
    return this.notificationService.getNotificationFromDatabase(id);
  }

  @Post()
  async sendNotification(
    @Body() notificationData: { from: number; to: number },
  ) {
    try {
      const notification =
        await this.notificationService.createNotification(notificationData);
      return notification;
    } catch (error) {
      console.error('Error sending notification:', error.message);
      return { success: false, error: error.message };
    }
  }
}
