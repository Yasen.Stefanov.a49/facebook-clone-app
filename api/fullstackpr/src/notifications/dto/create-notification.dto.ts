import { IsInt, IsNotEmpty } from 'class-validator';

export class CreateNotificationDto {
  @IsInt()
  @IsNotEmpty()
  to: number;

  @IsInt()
  @IsNotEmpty()
  from: number;
}
