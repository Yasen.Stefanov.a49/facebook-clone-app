import { Module } from '@nestjs/common';
import { NotificationService } from '../notifications/notification.service';
import { NotificationsController } from '../notifications/notification.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from 'src/user/entities/user.entity';
import { Notification } from './entity/notification.entity';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, Notification])],
  providers: [NotificationService],
  controllers: [NotificationsController],
  exports: [NotificationModule],
})
export class NotificationModule {}
