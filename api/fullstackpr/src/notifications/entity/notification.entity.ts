import { UserEntity } from 'src/user/entities/user.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

@Entity()
export class Notification {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'int' })
  to: number;

  @Column({ type: 'int' })
  from: number;

  @ManyToOne(() => UserEntity, (user) => user.toNotifications)
  toUser: UserEntity;

  @ManyToOne(() => UserEntity, (user) => user.fromNotifications)
  fromUser: UserEntity;
}
