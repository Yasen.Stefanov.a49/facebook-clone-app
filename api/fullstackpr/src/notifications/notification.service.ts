import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notifications/entity/notification.entity';

@Injectable()
export class NotificationService {
  constructor(
    @InjectRepository(Notification)
    private readonly notificationRepository: Repository<Notification>,
  ) {}

  async createNotification(notificationData: {
    from: number;
    to: number;
  }): Promise<Notification> {
    const { from, to } = notificationData;
    const notification: Notification = this.notificationRepository.create({
      from,
      to,
    } as Partial<Notification>);

    await this.notificationRepository.save(notification);
    return notification;
  }
  async getNotificationFromDatabase(id: number) {
    const notification = await this.notificationRepository.findOne({
      where: { id: id },
    });

    if (!notification) {
      throw new Error('Image not found');
    }

    return notification;
  }
  findAllNotifications = async (): Promise<Notification[]> => {
    try {
      const notifications = await this.notificationRepository.find();
      return notifications;
    } catch (error) {
      console.error('Error in findAllImages:', error);
      throw new Error('An error occurred while fetching images.');
    }
  };
}
