import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from 'src/user/entities/user.entity';
import { Notification } from 'src/notifications/entity/notification.entity';
import { PostEntity } from 'src/post/entity/post.entity';
import { NotificationService } from 'src/notifications/notification.service';
import { PostService } from 'src/post/post.service';
import { ImageService } from 'src/image/image.service';
import { ImageEntity } from '../image/entity/image.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserEntity,
      Notification,
      PostEntity,
      ImageEntity,
    ]),
  ],
  controllers: [UserController],
  providers: [UserService, ImageService, NotificationService, PostService],
})
export class UserModule {}
