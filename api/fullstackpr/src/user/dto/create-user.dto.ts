import {
  IsEmail,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsString,
  Matches,
  MinLength,
  IsArray,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

const passwordRegEx =
  /^(?=.*[a-z])(?=.*[A-Z])(?=.*d)(?=.*[@$!%*?&])[A-Za-zd@$!%*?&]{8,20}$/;

class FriendDto {
  @IsString()
  friendId: string;
}

class NotificationDto {
  @IsInt()
  to: number;

  @IsInt()
  from: number;
}

export class CreateUserDto {
  id: number;
  @IsString()
  @MinLength(2, { message: 'Name must have at least 2 characters.' })
  @IsNotEmpty()
  firstname: string;

  @IsString()
  @MinLength(2, { message: 'Name must have at least 2 characters.' })
  @IsNotEmpty()
  lastname: string;

  @IsNotEmpty()
  @IsEmail(null, { message: 'Please provide valid Email.' })
  email: string;

  @IsInt()
  age: number;

  @IsString()
  @IsEnum(['f', 'm', 'u'])
  gender: string;

  @IsNotEmpty()
  @Matches(passwordRegEx, {
    message: `Password must contain Minimum 8 and maximum 20 characters, 
    at least one uppercase letter, 
    one lowercase letter, 
    one number and 
    one special character`,
  })
  password: string;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => FriendDto)
  friends: FriendDto[];

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => NotificationDto)
  notifications: NotificationDto[];

  messages: any[];

  bio: string;

  livesIn: string;

  bornIn: string;

  worksAt: string;

  secondarySchool: string;

  university: string;

  relationship: string;
  @IsArray()
  hobbies: string[];
  avatarId: number | null;
  images: Buffer[];
  notificationId?: number;
  imageUrl: string;
}
