import {
  Column,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ImageEntity } from 'src/image/entity/image.entity';
import { Notification } from 'src/notifications/entity/notification.entity';
import { PostEntity } from 'src/post/entity/post.entity';

@Entity()
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 20 })
  firstname: string;

  @Column({ type: 'varchar', length: 20 })
  lastname: string;

  @Column({ type: 'varchar', length: 40 })
  email: string;

  @Column({ type: 'int' })
  age: number;

  @Column({ type: 'varchar' })
  password: string;

  @Column({ type: 'enum', enum: ['m', 'f', 'u'] })
  gender: string;

  @Column({ type: 'varchar', array: true, default: [] })
  friends: string[];

  @OneToMany(() => Notification, (notification) => notification.toUser)
  toNotifications: Notification[];

  @OneToMany(() => Notification, (notification) => notification.fromUser)
  fromNotifications: Notification[];

  @OneToMany(() => PostEntity, (post) => post.user)
  posts: PostEntity[];

  @ManyToMany(() => PostEntity, (post) => post.likedBy)
  likedPosts: PostEntity[];

  @ManyToMany(() => PostEntity, (post) => post.sharedBy)
  sharedPosts: PostEntity[];

  @Column({ type: 'jsonb', array: true, default: null, nullable: true })
  messages: any[];

  @Column({ type: 'varchar', length: 78, default: '' })
  bio: string;

  @Column({ type: 'varchar', length: 78, default: '' })
  livesIn: string;

  @Column({ type: 'varchar', length: 78, default: '' })
  bornIn: string;

  @Column({ type: 'varchar', length: 78, default: '' })
  worksAt: string;

  @Column({ type: 'varchar', length: 78, default: '' })
  secondarySchool: string;

  @Column({ type: 'varchar', length: 78, default: '' })
  university: string;

  @Column({ type: 'varchar', length: 40, default: '' })
  relationship: string;

  @Column({ type: 'varchar', array: true, default: [] })
  hobbies: string[];

  @OneToMany(() => ImageEntity, (image) => image.user)
  images: ImageEntity[];

  @Column({ nullable: true })
  public avatarId?: number;

  @Column({ nullable: true })
  public avatarUrl?: string;

  public notificationId?: number;
}
