import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UserEntity } from 'src/user/entities/user.entity';

/**
 * whatever the string pass in controller decorator it will be appended to
 * API URL. to call any API from this controller you need to add prefix which is
 * passed in controller decorator.
 * in our case our base URL is http://localhost:3000/user
 */
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post(':userId/avatar')
  async addAvatar(
    @Param('userId') userId: string,
    @Body('avatarId') avatarId: string,
  ) {
    const parsedUserId = Number(userId);
    const parsedAvatarId = Number(avatarId);
    return this.userService.addAvatar(parsedUserId, parsedAvatarId);
  }

  @Post(':userId/notifications')
  async addNotification(
    @Param('userId') userId: number,
    @Body() updateUserDto: Partial<UserEntity>,
  ) {
    return this.userService.updateUser(userId, updateUserDto);
  }

  @Get(':userId/images')
  async getUserImages(@Param('userId') userId: number) {
    return this.userService.findUserImages(userId);
  }
  @Get(':userId/notifications')
  async getUserNotifications(@Param('userId') userId: number) {
    return this.userService.userReceivedNotifications(userId);
  }
  @Get(':userId/friends')
  async getUserFriends(@Param('userId') userId: number) {
    return this.userService.getUserFriends(userId);
  }
  @Get(':userId/suggestedFriends')
  async getUserSuggestedFriends(@Param('userId') userId: number) {
    return this.userService.getUserSuggestedFriends(userId);
  }
  /**
   * Post decorator represents method of request as we have used post decorator the method
   * of this API will be post.
   * so the API URL to create User will be
   * POST http://localhost:3000/user
   */
  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.userService.createUser(createUserDto);
  }

  /**
   * we have used get decorator to get all the user's list
   * so the API URL will be
   * GET http://localhost:3000/user
   */
  @Get()
  findAll() {
    return this.userService.findAllUser();
  }

  /**
   * we have used get decorator with id param to get id from request
   * so the API URL will be
   * GET http://localhost:3000/user/:id
   */
  @Get(':userId')
  findOne(@Param('userId') id: number) {
    return this.userService.viewUser(id);
  }

  /**
   * we have used patch decorator with id param to get id from request
   * so the API URL will be
   * PATCH http://localhost:3000/user/:id
   */
  @Patch(':id')
  update(@Param('id') id: number, @Body() updatedValue: Partial<UserEntity>) {
    return this.userService.updateUser(id, updatedValue);
  }

  /**
   * we have used Delete decorator with id param to get id from request
   * so the API URL will be
   * DELETE http://localhost:3000/user/:id
   */
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userService.removeUser(+id);
  }
}
