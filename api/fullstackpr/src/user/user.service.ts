import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UserEntity } from 'src/user/entities/user.entity';
import { ImageService } from 'src/image/image.service';
import { ImageEntity } from 'src/image/entity/image.entity';

@Injectable()
export class UserService {
  /**
   * Here, we have used data mapper approach for this tutorial that is why we
   * injecting repository here. Another approach can be Active records.
   */
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(ImageEntity)
    private readonly imageRepository: Repository<ImageEntity>,
    private readonly imageFiles: ImageService,
  ) {}

  hashPassword(password: string) {
    let hash = 0;
    if (password.length === 0) return hash;

    for (let i = 0; i < password.length; i++) {
      const char = password.charCodeAt(i);
      hash = (hash << 5) - hash + char;
    }
    return String(hash);
  }
  /**
   * this is function is used to create User in User Entity.
   * @param createUserDto this will type of createUserDto in which
   * we have defined what are the keys we are expecting from body
   * @returns promise of user
   */
  async createUser(createUserDto: CreateUserDto): Promise<UserEntity> {
    const user: UserEntity = new UserEntity();
    user.firstname = createUserDto.firstname;
    user.lastname = createUserDto.lastname;
    user.age = createUserDto.age;
    user.email = createUserDto.email;
    user.password = createUserDto.password;
    user.gender = createUserDto.gender;

    return this.userRepository.save(user);
  }

  async addAvatar(userId: number, avatarId: number) {
    const user = await this.userRepository.findOne({
      where: { id: userId },
    });
    if (!user) {
      return null;
    }
    await this.userRepository
      .createQueryBuilder()
      .update(UserEntity)
      .set({ avatarId: avatarId })
      .where('id = :userId', { userId })
      .execute();

    return avatarId;
  }

  async findUserImages(userId: number) {
    const userImages = await this.imageRepository.find({
      where: { user: { id: userId } },
      relations: ['user'],
    });
    return userImages;
  }

  /**
   * this function is used to get all the user's list
   * @returns promise of array of users
   */
  async findAllUser(): Promise<UserEntity[]> {
    const users = await this.userRepository.find();
    return users;
  }

  /**
   * this function used to get data of use whose id is passed in parameter
   * @param id is type of number, which represent the id of user.
   * @returns promise of user
   */
  async viewUser(id: number): Promise<UserEntity> {
    const user = await this.userRepository.findOneBy({ id });
    delete user.password;
    if (user.avatarId) {
      const avatarImage = await this.imageFiles.getImageDataFromDatabase(
        user.avatarId,
      );
      const avatarImageUrl =
        this.imageFiles.convertByteArrayToImage(avatarImage);

      user.avatarUrl = avatarImageUrl;
    } else {
      user.avatarUrl = null;
    }
    return user;
  }

  async viewUsers(array: number[]): Promise<UserEntity[] | null> {
    try {
      const userPromises = await Promise.all(
        array.map(async (userId) => await this.viewUser(userId)),
      );

      return userPromises as UserEntity[];
    } catch (error) {
      return null;
    }
  }

  /**
   * this function is used to updated specific user whose id is passed in
   * parameter along with passed updated data
   * @param id is type of number, which represent the id of user.
   * @param updatedValue this is partial type of userEntity.
   * @returns promise of update user
   */
  updateUser(id: number, updatedValue: Partial<UserEntity>): void {
    const currentUser = this.userRepository.findOneBy({ id });
    console.log(updatedValue);
    this.userRepository.update(id, { ...currentUser, ...updatedValue });
  }

  /**
   * this function is used to remove or delete user from database.
   * @param id is the type of number, which represent id of user
   * @returns number of rows deleted or affected
   */
  removeUser(id: number): Promise<{ affected?: number }> {
    return this.userRepository.delete(id);
  }

  async userReceivedNotifications(id: number): Promise<UserEntity[] | null> {
    const user = await this.userRepository.findOne({
      where: { id: id },
      relations: ['toNotifications'],
    });
    console.log(user);
    if (!user) {
      return null;
    }
    const notifications = user.toNotifications || [];
    console.log(notifications);
    if (notifications.length === 0) {
      return null;
    } else {
      const usersArray = await Promise.all(
        notifications.map(async (notification) => {
          const sender = await this.viewUser(notification.from);
          return {
            ...sender,
            notificationId: notification.id,
          };
        }),
      );
      const convertedData = await this.convertUserData(usersArray);
      return convertedData;
    }
  }
  async getUserFriends(id: number): Promise<UserEntity[] | null> {
    const user = await this.userRepository.findOne({ where: { id: id } });
    if (!user) {
      return null;
    }
    const friends = user.friends || [];
    if (friends.length === 0) {
      return null;
    } else {
      const usersArray = await Promise.all(
        friends.map(async (personId) => {
          return await this.viewUser(+personId);
        }),
      );
      const convertedData = await this.convertUserData(usersArray);
      return convertedData;
    }
  }
  async convertUserData(data: UserEntity[]): Promise<UserEntity[]> {
    const resolvedData = await Promise.all(
      data.map(async (user: UserEntity) => {
        if (user.avatarId) {
          const avatar = await this.imageFiles.getImageDataFromDatabase(
            user.avatarId,
          );
          const imageUrl =
            this.imageFiles.convertByteArrayToImage(avatar!) || null;
          return { ...user, imageUrl };
        }
        return { ...user, imageUrl: null };
      }),
    );
    return resolvedData;
  }
  async getUserSuggestedFriends(currentUserId: number): Promise<UserEntity[]> {
    const allUsers = await this.userRepository.find();
    const currentUser = await this.userRepository.findOneBy({
      id: currentUserId,
    });
    const suggestedFriends = allUsers.filter(
      (user) =>
        user.id !== currentUser.id &&
        !user.friends.includes(currentUserId.toString()),
    );
    const suggestedFriendsCollection = await Promise.all(
      suggestedFriends.map(async (user: UserEntity) => {
        if (user.avatarId) {
          const avatar = await this.imageFiles.getImageDataFromDatabase(
            user.avatarId,
          );
          const imageUrl =
            this.imageFiles.convertByteArrayToImage(avatar!) || null;
          return { ...user, imageUrl };
        }
        return { ...user, imageUrl: null };
      }),
    );
    return suggestedFriendsCollection;
  }
}
