import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const config: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5173,
  username: 'postgres',
  password: 'keeptrying',
  database: 'master',
  synchronize: true,
};
