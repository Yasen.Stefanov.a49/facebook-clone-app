import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { ImageEntity } from './image/entity/image.entity';
import { UserEntity } from './user/entities/user.entity';
import { ImageModule } from './image/image.module';
import { Notification } from './notifications/entity/notification.entity';
import { NotificationsController } from './notifications/notification.controller';
import { NotificationModule } from './notifications/notification.module';
import { NotificationService } from './notifications/notification.service';
import { PostEntity } from './post/entity/post.entity';
import { PostModule } from './post/post.module';
import { PostService } from './post/post.service';
import { PostController } from './post/post.controller';
import { UserService } from './user/user.service';
import { ImageService } from './image/image.service';
import { UserController } from './user/user.controller';
import { ImageController } from './image/image.controller';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5173,
      schema: 'users',
      password: 'keeptrying',
      username: 'postgres',
      entities: [UserEntity, ImageEntity, Notification, PostEntity],
      database: 'master',
      synchronize: true,
      logging: true,
    }),
    TypeOrmModule.forFeature([
      Notification,
      UserEntity,
      ImageEntity,
      PostEntity,
    ]),
    UserModule,
    ImageModule,
    NotificationModule,
    PostModule,
  ],
  controllers: [
    NotificationsController,
    PostController,
    UserController,
    ImageController,
  ],
  providers: [
    AppService,
    UserService,
    ImageService,
    NotificationService,
    PostService,
  ],
  exports: [NotificationModule, PostModule, ImageModule, UserModule],
})
export class AppModule {}
