import axios from "axios";
import {
  UserFormProps,
  LoginFormProps,
  FileProps,
  UserProps,
  PostProps,
} from "../types/types";

const API_URL = "http://localhost:3000";

const api = axios.create({
  baseURL: API_URL,
});

export const createUser = async (data: UserFormProps): Promise<Response> => {
  const userInformation = {
    ...data,
  };

  const response = await api.post("/user", userInformation);

  return response.data;
};

export const logUser = async (
  incommingData: LoginFormProps
): Promise<object | null> => {
  const response = await api.get("/user");
  const currentUser = response.data.find(
    (user: UserFormProps) =>
      user.email === incommingData.email &&
      user.password === incommingData.password
  );
  if (currentUser) {
    console.log(currentUser);

    return currentUser;
  } else {
    console.log("no such user!");
    return null;
  }
};

export const getUser = async (userId: number): Promise<UserProps> => {
  const user = await api.get(`/user/${userId}`);
  return { ...user.data };
};

export const getAllUsers = async (): Promise<UserProps[]> => {
  const response = await api.get(`/user`);
  return response.data;
};

export const getUserFriends = async (userId: number): Promise<UserProps[]> => {
  const response = await api.get(`/user/${userId}/friends`);
  return response.data;
};

export const getUserSuggestedFriends = async (
  userId: number
): Promise<UserProps[]> => {
  const response = await api.get(`/user/${userId}/suggestedFriends`);
  return response.data;
};

export const updateUserProperty = async (
  userId: number,
  propertyName: string,
  updatedValue: string | string[] | File | number
) => {
  try {
    const user = await getUser(userId);
    if (propertyName === "images" && updatedValue instanceof File) {
      const formData = new FormData();
      formData.append("file", updatedValue);

      const response = await api.post(`user/${userId}/avatar`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });

      if (response.status === 201) {
        console.log("Image added successfully.");
      } else {
        console.error("Image upload failed:", response.data);
      }
    } else {
      if (Array.isArray(updatedValue)) {
        user[propertyName] = [...user[propertyName], ...updatedValue];
      } else {
        user[propertyName] = updatedValue;
      }

      const updatePayload = {
        [propertyName]: user[propertyName],
      };
      console.log(updatePayload);

      const response = await api.patch(`user/${userId}`, updatePayload);

      if (response.status === 200) {
        console.log(`${propertyName} updated successfully.`);
      } else {
        console.error(`${propertyName} update failed:`, response.data);
      }
    }
  } catch (error) {
    console.error("An error occurred:", error);
  }
};

export const uploadImage = async (
  userId: number,
  file: File
): Promise<number | null> => {
  const formData = new FormData();
  formData.append("file", file);

  try {
    const response = await api.post(`/images/${userId}`, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
    const imageId = response.data.id;
    return imageId;
  } catch (error: unknown) {
    console.error("Error uploading image:");
    return null;
  }
};

export const changeAvatar = async (
  userId: number,
  avatarId: number
): Promise<void> => {
  const formData = new FormData();
  formData.append("userId", String(userId));
  formData.append("avatarId", String(avatarId));

  try {
    const response = await api.post(`/user/${userId}/avatar`, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });

    console.log(response.data);
  } catch (error) {
    console.error("Error uploading image:");
  }
};

export const getUserImages = async (
  userId: number
): Promise<Response | null> => {
  try {
    const response = await api.get(`/user/${userId}/images`, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });

    return response.data;
  } catch (error) {
    console.error("Error fetching user images:");
    return null;
  }
};

export const getAllImages = async (): Promise<Response | null> => {
  try {
    const response = await api.get(`/images`, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });

    return response.data;
  } catch (error) {
    return null;
  }
};

export const getUserAvatar = async (
  userId: number
): Promise<FileProps | null> => {
  try {
    if (!userId) {
      return null;
    }
    const response = await api.get(`/images/${userId}`);
    return response.data;
  } catch (error) {
    return null;
  }
};

export const convertByteArrayToImage = (file: FileProps): string | null => {
  try {
    const byteArray = new Uint8Array(file.imageData.data);
    const blob = new Blob([byteArray], { type: "image/jpeg" });
    const dataUrl = URL.createObjectURL(blob);
    return dataUrl;
  } catch (error) {
    return null;
  }
};

export const createNotification = async (
  userId: number,
  recipientId: number
): Promise<Response | null> => {
  try {
    const response = await api.post(
      `/notifications/`,
      { from: userId, to: recipientId },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    return response.data;
  } catch (error) {
    console.error("Error creating notification:");
    return null;
  }
};

export const getUserReceivedNotifications = async (
  userId: number
): Promise<UserProps[] | null> => {
  try {
    const response = await api.get(`/user/${userId}/notifications`);
    console.log(response);
    return response.data;
  } catch (error) {
    console.error("Error getting notifications");
    return null;
  }
};

export const createPost = async (
  userId: number,
  data: string
): Promise<PostProps | null> => {
  try {
    const response = await api.post(`/posts`, { userId, data });
    return response.data;
  } catch (error) {
    console.error("Error creating post:");
    return null;
  }
};
export const getPost = async (postId: number): Promise<PostProps | null> => {
  try {
    const response = await api.get(`/posts/${postId}`);
    return response.data;
  } catch (error) {
    console.error("Error getting post");
    return null;
  }
};

export const getAllPosts = async (): Promise<UserProps[] | null> => {
  try {
    const response = await api.get(`/posts`);
    return response.data;
  } catch (error) {
    console.error("Error getting posts");
    return null;
  }
};

export const likePost = async (
  userId: number,
  postId: number,
  action: string
): Promise<PostProps | null> => {
  try {
    const postResponse = await api.get(`/posts/${postId}`);
    const post = postResponse.data;

    if (post) {
      post.likedBy = [...post.likedBy, userId];
    }

    const updatePayload = {
      userId,
      action,
      likedBy: [...post.likedBy, userId],
    };

    const response = await api.patch(`posts/${postId}`, updatePayload);
    console.log(response.data);

    return response.data;
  } catch (error) {
    console.error("Error liking post");
    return null;
  }
};

export const sharePost = async (
  userId: number,
  postId: number,
  action: string
): Promise<PostProps | null> => {
  try {
    const postResponse = await api.get(`/posts/${postId}`);
    const post = postResponse.data;
    console.log(post);

    if (post) {
      post.sharedBy = [...(post.sharedBy || []), userId];
    }

    const updatePayload = {
      userId,
      action,
      sharedBy: [...post.sharedBy, userId],
    };

    const response = await api.patch(`posts/${postId}`, updatePayload);
    console.log(response.data);

    return response.data;
  } catch (error) {
    console.error("Error sharing post", error.message);
    return null;
  }
};

export const deleteNotificationFromDatabase = async (): Promise<void> => {};
