import { NavigateFunction, useNavigate } from "react-router-dom";
import { NavigationProps } from "../types/types";
const useNavigation = (): NavigationProps => {
  const navigate: NavigateFunction = useNavigate();

  const handleNavigation = (
    path: string,
    state?: object | null | undefined
  ): void => {
    navigate(path, { state });
  };

  return { handleNavigation };
};

export default useNavigation;
