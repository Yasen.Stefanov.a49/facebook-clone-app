// userSlice.js
import { createSlice } from "@reduxjs/toolkit";

const getUserFromLocalStorage = () => {
  const storedUser = localStorage.getItem("currentUser");

  try {
    return storedUser ? JSON.parse(storedUser) : null;
  } catch (error) {
    console.error("Error parsing user from localStorage:", error);
    return null;
  }
};

export const userSlice = createSlice({
  name: "user",
  initialState: {
    user: getUserFromLocalStorage(),
  },
  reducers: {
    login: (state, action) => {
      state.user = action.payload;
      localStorage.setItem("currentUser", JSON.stringify(action.payload));
    },
    logout: (state) => {
      state.user = null;
      localStorage.removeItem("currentUser");
    },
  },
});

export const { login, logout } = userSlice.actions;
export const selectUser = (state) => state.user.user;
export default userSlice.reducer;
