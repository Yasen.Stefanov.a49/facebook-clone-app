import "./App.css";
import Header from "./components/header";
import { BrowserRouter as Router } from "react-router-dom";
import Routing from "./routing/routes";
import { QueryClient, QueryClientProvider } from "react-query";
import "reflect-metadata";

function App() {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <Header />
        <Routing />
      </Router>
    </QueryClientProvider>
  );
}

export default App;
