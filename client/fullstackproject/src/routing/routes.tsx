import { Route, Routes } from "react-router-dom";
import Register from "../components/register/index.tsx";
import Login from "../components/login/index.tsx";
import { Profile } from "../views/profile/index.tsx";
import { Home } from "../components/home/index.tsx";
const Routing = () => {
  return (
    <Routes>
      <Route path="/sign-up" element={<Register />} />
      <Route path="/sign-in" element={<Login />} />
      <Route path="/" element={<Home />} />
      <Route path="/profile/:id" element={<Profile />} />
    </Routes>
  );
};

export default Routing;
