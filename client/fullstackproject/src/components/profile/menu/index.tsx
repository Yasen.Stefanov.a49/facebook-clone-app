import * as React from "react";
import { useSelector, useDispatch } from "react-redux";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { selectUser } from "../../../features/userSlice";
import Divider from "@mui/material/Divider";
import { Box } from "@mui/material";
import { Button } from "@mui/material";
import SettingsIcon from "@mui/icons-material/Settings";
import HelpIcon from "@mui/icons-material/Help";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import FeedbackIcon from "@mui/icons-material/Feedback";
import LogoutIcon from "@mui/icons-material/Logout";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import ListItemIcon from "@mui/material/ListItemIcon";
import { logout } from "../../../features/userSlice";
import useNavigation from "../../../hooks/navigate";

export const BasicMenu: React.FC = ({
  isAvatarClicked,
  setIsAvatarClicked,
}) => {
  const { handleNavigation } = useNavigation();
  const currentUser = useSelector(selectUser);
  const dispatch = useDispatch();
  const handleClose = (): void => {
    setIsAvatarClicked(!isAvatarClicked);
  };

  const handleLogout = (): void => {
    handleNavigation("/");
    dispatch(logout());
  };

  return (
    <Menu
      anchorEl={null}
      open={isAvatarClicked}
      onClose={handleClose}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      PaperProps={{
        sx: {
          background: "#3a3b3c",
        },
      }}
      sx={{ top: 30 }}
    >
      <Box sx={{ display: "flex", flexDirection: "column", width: 300 }}>
        <Box
          sx={{
            border: 0.1,
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            borderColor: "#3a3b3c",
            borderRadius: 2,
            marginLeft: 1,
            marginRight: 1,
          }}
        >
          <MenuItem sx={{ color: "#e4e6eb" }} onClick={handleClose}>
            <Button
              sx={{
                "&:hover": {
                  backgroundColor: "#555759",
                },
              }}
            >
              {currentUser?.firstname} {currentUser?.lastname}
            </Button>
          </MenuItem>
          <Divider sx={{ background: "#e4e6eb" }} variant="middle"></Divider>
          <MenuItem sx={{ color: "blue" }} onClick={handleClose}>
            <Button
              sx={{
                "&:hover": {
                  backgroundColor: "#555759",
                },
              }}
            >
              See all profiles
            </Button>
          </MenuItem>
        </Box>
        <MenuItem
          onClick={handleClose}
          sx={{
            color: "#e4e6eb",
            marginLeft: 1,
            marginRight: 1,
            "&:hover": {
              backgroundColor: "#555759",
              borderRadius: "8px",
            },
          }}
        >
          {" "}
          <ListItemIcon>
            <SettingsIcon sx={{ color: "#e4e6eb" }} />
          </ListItemIcon>
          Settings & privacy
          <ArrowForwardIosIcon sx={{ position: "absolute", right: 6 }} />
        </MenuItem>
        <MenuItem
          onClick={handleClose}
          sx={{
            color: "#e4e6eb",
            marginLeft: 1,
            marginRight: 1,
            "&:hover": {
              backgroundColor: "#555759",
              borderRadius: "8px",
            },
          }}
        >
          <ListItemIcon>
            <HelpIcon sx={{ color: "#e4e6eb" }} />
          </ListItemIcon>
          Help & support
          <ArrowForwardIosIcon sx={{ position: "absolute", right: 6 }} />
        </MenuItem>
        <MenuItem
          onClick={handleClose}
          sx={{
            color: "#e4e6eb",
            marginLeft: 1,
            marginRight: 1,
            "&:hover": {
              backgroundColor: "#555759",
              borderRadius: "8px",
            },
          }}
        >
          {" "}
          <ListItemIcon>
            <DarkModeIcon sx={{ color: "#e4e6eb" }} />
          </ListItemIcon>
          Display & accessibility
          <ArrowForwardIosIcon sx={{ position: "absolute", right: 6 }} />
        </MenuItem>
        <MenuItem
          onClick={handleClose}
          sx={{
            color: "#e4e6eb",
            marginLeft: 1,
            marginRight: 1,
            "&:hover": {
              backgroundColor: "#555759",
              borderRadius: "8px",
            },
          }}
        >
          {" "}
          <ListItemIcon>
            <FeedbackIcon sx={{ color: "#e4e6eb" }} />
          </ListItemIcon>
          Give feedback
        </MenuItem>
        <MenuItem
          onClick={handleLogout}
          sx={{
            color: "#e4e6eb",
            marginLeft: 1,
            marginRight: 1,
            "&:hover": {
              backgroundColor: "#555759",
              borderRadius: "8px",
            },
          }}
        >
          <ListItemIcon>
            <LogoutIcon sx={{ color: "#e4e6eb" }} />
          </ListItemIcon>
          Log out
        </MenuItem>
      </Box>
    </Menu>
  );
};
