import Avatar from "@mui/material/Avatar";
import ListItemText from "@mui/material/ListItemText";
import { Box, Divider, Typography } from "@mui/material";
import { getUser, getUserFriends } from "../../api/api";
import { useQuery } from "react-query";
import { useSelector } from "react-redux";
import { selectUser } from "../../features/userSlice";
import { UserProps } from "../../types/types";
import CircularIndeterminate from "../spinner/spinner";

export const FriendsContainer: React.FC = () => {
  const currentUser = useSelector(selectUser);

  const { data: currentUserInfo } = useQuery<UserProps>(
    "userInformation",
    async () => await getUser(currentUser?.id),
    {
      enabled: !!currentUser,
    }
  );

  const {
    data: friends,
    isSuccess,
    isFetching,
  } = useQuery<UserProps[]>(
    "friends",
    async () => (await getUserFriends(currentUser.id)) || [],
    {
      enabled: !!currentUserInfo,
      onSuccess: (data) => {
        console.log(data);
      },
    }
  );

  return (
    <Box
      sx={{
        marginRight: 39,
        background: "#242526",
        width: 340,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        borderRadius: 2,
      }}
    >
      <Typography variant="h5" sx={{ color: "white" }}>
        Contacts
      </Typography>
      <Divider
        sx={{
          background: "#e4e6eb",
          height: 1,
          width: "90%",
        }}
        variant="middle"
      ></Divider>
      {friends?.length === 0 && (
        <Typography sx={{ color: "white" }}>
          You do not have any contacts yet.
        </Typography>
      )}
      <Box
        sx={{
          maxHeight: "300px",
          overflowY: "scroll",
          "&::-webkit-scrollbar": {
            width: "0.5em",
          },
        }}
      >
        {isFetching ? (
          <CircularIndeterminate />
        ) : (
          <>
            {isSuccess &&
              friends.map((user: UserProps, index: number) => (
                <Box key={index}>
                  <Box
                    sx={{
                      width: "100%",
                      maxWidth: 250,
                      borderRadius: "16px",
                      boxShadow: "0px 4px 8px rgba(0, 0, 0, 0.1)",
                      backgroundColor: "background.paper",
                      display: "flex",
                      alignItems: "center",
                      padding: "8px",
                      mt: 0.5,
                      background: "#555759",
                      gap: 2,
                    }}
                  >
                    <Avatar src={user.imageUrl} />
                    <ListItemText
                      primary={`${user.firstname} ${user.lastname}`}
                      sx={{ flex: 1, paddingRight: "16px", width: 170 }}
                    />
                  </Box>
                </Box>
              ))}
          </>
        )}
      </Box>
    </Box>
  );
};
