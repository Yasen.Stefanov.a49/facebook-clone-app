import React from "react";
import { useSelector } from "react-redux";
import { Box } from "@mui/material";
import { FeaturesBar } from "../options/features";
import { CreatePost } from "../createpost";
import { selectUser } from "../../features/userSlice";
import { SuggestedFriends } from "../suggested friends";
import { FriendsContainer } from "../friends/container";
import { PostContainer } from "../post-container";
export const Home: React.FC = () => {
  const currentUser = useSelector(selectUser);

  return (
    <Box
      sx={{
        width: "1920px",
        height: "auto",
        backgroundColor: "#18191a",
      }}
    >
      {currentUser && (
        <>
          <FeaturesBar />
          <Box
            sx={{
              display: "flex",
              alignItems: "flex-start",
              justifyContent: "space-between",
            }}
          >
            <SuggestedFriends />
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "column",
                gap: 1,
              }}
            >
              <CreatePost />
              <PostContainer />
            </Box>
            <FriendsContainer />
          </Box>
        </>
      )}
    </Box>
  );
};
