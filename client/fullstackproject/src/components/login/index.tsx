import { TextField, Box, Button, Typography } from "@mui/material";
import { useForm } from "react-hook-form";
import { logUser } from "../../api/api";
import { LoginFormProps } from "../../types/types";
import { useDispatch } from "react-redux/es/exports";
import { login } from "../../features/userSlice";
import useNavigation from "../../hooks/navigate";

const Login: React.FC = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      email: "",
      password: "",
    },
  });
  const { handleNavigation } = useNavigation();
  const dispatch = useDispatch();

  const onSubmit = async (data: LoginFormProps) => {
    try {
      const userLogin = await logUser(data);
      console.log(userLogin);

      dispatch(login(userLogin));
      console.log("User logged:", userLogin);

      handleNavigation("/");
    } catch (error) {
      console.error("Error logging user:", error);
    }
  };

  return (
    <Box
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#242526",
        height: "100vh",
      }}
    >
      <form
        onSubmit={handleSubmit(onSubmit)}
        style={{
          width: 400,
          backgroundColor: "#3a3b3c",
          borderRadius: 20,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          gap: 6,
          color: "white",
          margin: "auto",
          marginTop: "30vh",
        }}
      >
        <Typography style={{ marginTop: 10 }} variant="h6">
          Login
        </Typography>
        <TextField
          style={{ marginTop: 10 }}
          id="email"
          label="Email"
          InputProps={{ style: { color: "white" } }}
          InputLabelProps={{ style: { color: "white" } }}
          {...register("email", { required: "This is required." })}
        />

        <TextField
          sx={{ marginTop: 1, color: "white" }}
          id="password"
          label="Password"
          type="password"
          InputProps={{ style: { color: "white" } }}
          InputLabelProps={{ style: { color: "white" } }}
          {...register("password", { required: "This is required." })}
        />
        <Button style={{ marginBottom: 20 }} variant="contained" type="submit">
          Login
        </Button>
      </form>
    </Box>
  );
};

export default Login;
