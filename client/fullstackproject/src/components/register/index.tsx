import { useForm } from "react-hook-form";
import { createUser } from "../../api/api";
import {
  TextField,
  RadioGroup,
  Box,
  Radio,
  FormControlLabel,
  FormControl,
  Button,
  Typography,
} from "@mui/material";
import { UserFormProps } from "../../types/types";
import { login } from "../../features/userSlice";
import useNavigation from "../../hooks/navigate";
import { useDispatch } from "react-redux/es/exports";

const Register: React.FC = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      firstname: "",
      lastname: "",
      email: "",
      age: "",
      password: "",
      gender: "m",
    },
  });
  const { handleNavigation } = useNavigation();
  const dispatch = useDispatch();

  const onSubmit = async (data: UserFormProps): Promise<void> => {
    try {
      const userLogin = await createUser(data);
      console.log(userLogin);

      dispatch(login(userLogin));
      console.log("User logged:", userLogin);

      handleNavigation("/");
    } catch (error) {
      console.error("Error creating user:", error);
    }
  };

  return (
    <Box
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#242526",
        height: "100vh",
      }}
    >
      <form
        onSubmit={handleSubmit(onSubmit)}
        style={{
          width: 400,
          borderRadius: 20,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          gap: 6,
          background: "#3a3b3c",
        }}
      >
        <Typography style={{ marginTop: 10, color: "white" }} variant="h6">
          Register
        </Typography>
        <TextField
          style={{ marginTop: 10 }}
          id="firstname"
          label="Firstname"
          InputProps={{ style: { color: "white" } }}
          InputLabelProps={{ style: { color: "white" } }}
          {...register("firstname", { required: "This is required." })}
        />
        {errors.firstname?.message}
        <TextField
          id="lastname"
          label="Lastname"
          InputProps={{ style: { color: "white" } }}
          InputLabelProps={{ style: { color: "white" } }}
          {...register("lastname", { required: "This is required." })}
        />
        <TextField
          id="email"
          label="Email"
          InputProps={{ style: { color: "white" } }}
          InputLabelProps={{ style: { color: "white" } }}
          {...register("email", { required: "This is required." })}
        />
        {errors.email?.message}
        <TextField
          id="age"
          label="Age"
          InputProps={{ style: { color: "white" } }}
          InputLabelProps={{ style: { color: "white" } }}
          {...register("age", { required: "This is required." })}
        />
        <TextField
          id="password"
          label="Password"
          InputProps={{ style: { color: "white" } }}
          InputLabelProps={{ style: { color: "white" } }}
          {...register("password", { required: "This is required." })}
          type="password"
        />
        <FormControl component="fieldset">
          <RadioGroup aria-label="gender" name="gender">
            <FormControlLabel value="male" control={<Radio />} label="Male" />
            <FormControlLabel
              value="female"
              control={<Radio />}
              label="Female"
            />
          </RadioGroup>
        </FormControl>
        <Button style={{ marginBottom: 20 }} variant="contained" type="submit">
          Register
        </Button>
      </form>
    </Box>
  );
};

export default Register;
