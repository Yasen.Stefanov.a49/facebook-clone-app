import Box from "@mui/material/Box";
import { selectUser } from "../../features/userSlice.ts";
import { useSelector } from "react-redux";
import Divider from "@mui/material/Divider";
import { Avatar, Button } from "@mui/material";
import CameraAltIcon from "@mui/icons-material/CameraAlt";
import InsertPhotoIcon from "@mui/icons-material/InsertPhoto";
import EmojiEmotionsIcon from "@mui/icons-material/EmojiEmotions";
import Typography from "@mui/material/Typography";
import { WhatsOnYourMind } from "./whatsonyourmind.tsx";
import { getUser } from "../../api/api.ts";
import { useQuery } from "react-query";
import { UserProps } from "../../types/types.ts";

export const CreatePost: React.FC = () => {
  const currentUser = useSelector(selectUser);
  const { data: userInformation } = useQuery<UserProps>(
    "userInfo",
    async () => await getUser(currentUser.id),
    {
      enabled: !!currentUser,
    }
  );
  const buttonOptionsForCreatePost = [
    {
      name: "Live Video",
      icon: CameraAltIcon,
      color: "red",
      fn: () => {
        console.log("Live Video");
      },
    },
    {
      name: "Photo/Video",
      icon: InsertPhotoIcon,
      color: "green",
      fn: () => {
        console.log("Photo/Video");
      },
    },
    {
      name: "Feeling/activity",
      icon: EmojiEmotionsIcon,
      color: "orange",
      fn: () => {
        console.log("Feeling/activity");
      },
    },
  ];
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: 600,
          borderRadius: "10px",
          alignItems: "center",
          justifyContent: "space-between",
          background: "#242526",
        }}
      >
        <Box
          sx={{
            display: "flex",
            height: 70,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            gap: 2,
          }}
        >
          <Avatar
            src={userInformation?.avatarUrl}
            sx={{ marginLeft: 25 }}
          ></Avatar>
          <WhatsOnYourMind />
        </Box>
        <Divider
          sx={{
            background: "#e4e6eb",
            height: 1,
            width: "90%",
          }}
          variant="middle"
        ></Divider>
        <Box
          sx={{
            height: 50,
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            gap: 1,
          }}
        >
          {buttonOptionsForCreatePost.map((option, i) => (
            <Button key={i} onClick={option.fn} sx={{ width: 190, gap: 0.5 }}>
              <option.icon sx={{ color: option.color }} />
              <Typography sx={{ color: "#e4e6eb", fontSize: 14 }}>
                {option.name}
              </Typography>
            </Button>
          ))}
        </Box>
      </Box>
    </Box>
  );
};
