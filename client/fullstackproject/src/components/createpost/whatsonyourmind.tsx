import { styled, alpha } from "@mui/material/styles";
import { selectUser } from "../../features/userSlice.ts";
import { useSelector } from "react-redux";
import InputBase from "@mui/material/InputBase";
import { useState } from "react";

import CancelIcon from "@mui/icons-material/Cancel";
import {
  Box,
  Button,
  Divider,
  Fab,
  Modal,
  TextareaAutosize,
  Typography,
  Paper,
  Avatar,
} from "@mui/material";
import { createPost, getUser, updateUserProperty } from "../../api/api.ts";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { UserProps } from "../../types/types.ts";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  height: 30,
  borderRadius: 30,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: 480,
  marginRight: 200,
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "White",
  "& .MuiInputBase-input": {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      "&:focus": {},
    },
  },
}));

export const WhatsOnYourMind: React.FC = () => {
  const queryClient = useQueryClient();
  const currentUser = useSelector(selectUser);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const handleOpen = (): void => setIsOpen(true);
  const handleClose = (): void => setIsOpen(false);
  const [postData, setPostData] = useState<string>("");
  const handleCreatePost = async (): Promise<void> => {
    await mutatePosts.mutateAsync(postData, {
      onSuccess: () => {
        queryClient.invalidateQueries("posts");
      },
    });
    handleClose();
  };
  const { data: userInformation } = useQuery<UserProps>(
    "userInfo",
    async () => await getUser(currentUser.id),
    {
      enabled: !!currentUser,
    }
  );

  const mutatePosts = useMutation(async (postData: string) => {
    await createPost(currentUser.id, postData);
  });

  return (
    <Box>
      <Search onClick={handleOpen}>
        <StyledInputBase
          placeholder={`Whats on your mind, ${currentUser?.firstname}`}
          inputProps={{ "aria-label": "search" }}
        />
      </Search>
      <Modal open={isOpen} onClose={handleClose}>
        <Paper
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: 400,
            backgroundColor: "#3a3b3c",
            color: "white",
            p: 2,
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <Typography variant="h6" className="modal-title">
              Create post
            </Typography>
            <Fab size="small" onClick={handleClose}>
              <CancelIcon />
            </Fab>
          </Box>
          <Divider sx={{ background: "white", margin: 1 }} />
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Box>
              <Box
                sx={{
                  display: "flex",
                  gap: 1,
                  alignItems: "center",
                  marginBottom: 1,
                }}
              >
                <Avatar src={userInformation?.avatarUrl}></Avatar>
                {currentUser.firstname} {currentUser.lastname}
              </Box>
              <TextareaAutosize
                value={postData}
                placeholder={`What's on your mind, ${currentUser.firstname}?`}
                maxRows={3}
                maxLength={75}
                style={{
                  marginTop: 1,
                  borderRadius: 5,
                  background: "#555759",
                  color: "white",
                  width: 340,
                  minWidth: 350,
                  maxWidth: 350,
                  minHeight: 100,
                  maxHeight: 100,
                }}
                onChange={(e) => setPostData(e.target.value)}
              ></TextareaAutosize>
            </Box>
          </Box>
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <Button variant="contained" onClick={handleCreatePost}>
              Post
            </Button>
          </Box>
        </Paper>
      </Modal>
    </Box>
  );
};
