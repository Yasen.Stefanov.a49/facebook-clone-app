import { Avatar, Box, Divider, ListItemIcon, MenuItem } from "@mui/material";
import { selectUser } from "../../features/userSlice.ts";
import { useSelector } from "react-redux";
import Diversity1Icon from "@mui/icons-material/Diversity1";
import QueryBuilderIcon from "@mui/icons-material/QueryBuilder";
import SaveIcon from "@mui/icons-material/Save";
import GroupIcon from "@mui/icons-material/Group";
import SlowMotionVideoIcon from "@mui/icons-material/SlowMotionVideo";
import ArrowDropDownCircleIcon from "@mui/icons-material/ArrowDropDownCircle";
import useNavigation from "../../hooks/navigate.ts";
import { getUser } from "../../api/api.ts";
import { useQuery } from "react-query";

export const FeaturesBar: React.FC = () => {
  const { handleNavigation } = useNavigation();
  const options = [
    {
      name: "Friends",
      icon: Diversity1Icon,
    },
    {
      name: "Memories",
      icon: QueryBuilderIcon,
    },
    {
      name: "Saved",
      icon: SaveIcon,
    },
    {
      name: "Groups",
      icon: GroupIcon,
    },
    {
      name: "Video",
      icon: SlowMotionVideoIcon,
    },
    {
      name: "See more",
      icon: ArrowDropDownCircleIcon,
    },
  ];
  const currentUser = useSelector(selectUser);
  const { data: userInformation } = useQuery(
    "userInfo",
    async () => await getUser(currentUser.id),
    {
      enabled: !!currentUser,
    }
  );

  return (
    <Box
      sx={{
        marginLeft: "300px",
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        width: 350,
      }}
    >
      <MenuItem
        onClick={() => handleNavigation(`profile/${currentUser.id}`)}
        sx={{
          gap: 1,
          color: "#e4e6eb",
          marginLeft: 1,
          marginRight: 1,
          "&:hover": {
            backgroundColor: "#555759",
            borderRadius: "8px",
          },
        }}
      >
        <Avatar
          src={userInformation?.avatarUrl}
          sx={{ width: 32, height: 32 }}
        ></Avatar>
        {currentUser?.firstname} {currentUser?.lastname}
      </MenuItem>
      {options.map((option, index) => (
        <MenuItem
          key={index}
          sx={{
            color: "#e4e6eb",
            marginLeft: 1,
            marginRight: 1,
            "&:hover": {
              backgroundColor: "#555759",
              borderRadius: "8px",
            },
          }}
        >
          {" "}
          <ListItemIcon>
            <option.icon sx={{ color: "#e4e6eb" }} />
          </ListItemIcon>
          {option.name}
        </MenuItem>
      ))}
      <Divider sx={{ background: "#e4e6eb" }} variant="middle"></Divider>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      ></Box>
    </Box>
  );
};
