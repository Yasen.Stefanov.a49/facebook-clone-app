import { selectUser } from "../../features/userSlice.ts";
import { useSelector } from "react-redux";
import ThumbUpAltIcon from "@mui/icons-material/ThumbUpAlt";
import CommentIcon from "@mui/icons-material/Comment";
import ShareIcon from "@mui/icons-material/Share";
import { Box, Button, Divider, Typography, Avatar } from "@mui/material";
import { getAllPosts, likePost, sharePost } from "../../api/api.ts";
import { useQuery } from "react-query";
import { UserProps } from "../../types/types.ts";
import CircularIndeterminate from "../spinner/spinner.tsx";

export const PostContainer: React.FC = () => {
  const currentUser = useSelector(selectUser);
  const {
    data: posts,
    isFetching,
    isFetched,
  } = useQuery<UserProps[] | null>(
    "posts",
    async () => (await getAllPosts()) || [],
    {
      enabled: !!currentUser,
      onSuccess: (data) => console.log(data),
    }
  );

  const postButtons = [
    {
      name: "Like",
      icon: ThumbUpAltIcon,
      fn: async (userId: number, postId: number) => {
        await likePost(userId, postId, "like");
      },
    },
    {
      name: "Comment",
      icon: CommentIcon,
      fn: () => {
        console.log("Photo/Video");
      },
    },
    {
      name: "Share",
      icon: ShareIcon,
      fn: async (userId: number, postId: number) => {
        await sharePost(userId, postId, "share");
      },
    },
  ];
  return (
    <Box>
      {isFetching ? (
        <CircularIndeterminate />
      ) : (
        <Box>
          {isFetched &&
            posts?.map((post) => (
              <Box
                key={post.postId}
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                  width: 600,
                  height: "auto",
                  background: "#242526",
                  borderRadius: "8px",
                  mb: 1,
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "flex-start",
                    width: "inherit",
                    marginLeft: "65px",
                    marginTop: "16px",
                    gap: 1,
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      flexDirection: "row",
                      gap: 1,
                    }}
                  >
                    <Avatar src={post.imageUrl} />
                    <Typography sx={{ color: "white" }}>
                      {post.firstname} {post.lastname}
                    </Typography>
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Typography
                    sx={{
                      color: "white",
                    }}
                  >
                    {post.data}
                  </Typography>
                  <Box
                    sx={{
                      display: "flex",
                      width: 600,
                      justifyContent: "space-around",
                      color: "white",
                    }}
                  >
                    <Typography>{post.likedBy.length} likes</Typography>
                    <Typography>comments</Typography>
                    <Typography>{post.sharedBy?.length} shares</Typography>
                  </Box>
                </Box>

                <Divider
                  sx={{
                    background: "#e4e6eb",
                    height: 1,
                    width: "95%",
                  }}
                  variant="middle"
                ></Divider>
                <Box>
                  {postButtons.map((button, i) => (
                    <Button
                      key={i}
                      onClick={() => {
                        button.fn(currentUser.id, post.postId!);
                      }}
                      sx={{ width: 190, gap: 0.5 }}
                    >
                      <button.icon />
                      <Typography sx={{ color: "#e4e6eb", fontSize: 14 }}>
                        {button.name}
                      </Typography>
                    </Button>
                  ))}
                </Box>
              </Box>
            ))}
        </Box>
      )}
    </Box>
  );
};
