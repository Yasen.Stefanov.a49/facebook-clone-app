import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import useNavigation from "../../hooks/navigate.ts";
import { selectUser } from "../../features/userSlice.ts";
import { useSelector } from "react-redux";
import { Avatar, Fab } from "@mui/material";
import NotificationsIcon from "@mui/icons-material/Notifications";
import MessageIcon from "@mui/icons-material/Message";
import MenuIcon from "@mui/icons-material/Menu";
import HomeIcon from "@mui/icons-material/Home";
import PeopleIcon from "@mui/icons-material/People";
import OndemandVideoIcon from "@mui/icons-material/OndemandVideo";
import StorefrontIcon from "@mui/icons-material/Storefront";
import Diversity3Icon from "@mui/icons-material/Diversity3";
import FacebookIcon from "@mui/icons-material/Facebook";
import { useQuery } from "react-query";
import { useState } from "react";
import SearchAppBar from "./search.tsx";
import { getUser } from "../../api/api.ts";
import { BasicMenu } from "../profile/menu/index.tsx";
import { NotificationsContainer } from "../notifications/container.tsx";

const Header: React.FC = () => {
  const { handleNavigation } = useNavigation();
  const currentUser = useSelector(selectUser);

  const [isAvatarClicked, setIsAvatarClicked] = useState<boolean>(false);
  const [isNotificationsClicked, setIsNotificationsClicked] =
    useState<boolean>(false);

  const handleAvatarClick = (): void => {
    setIsAvatarClicked(!isAvatarClicked);
  };

  const handleNotificationsIconClick = (): void => {
    setIsNotificationsClicked(!isNotificationsClicked);
  };

  const { data: userInformation } = useQuery(
    "userInfo",
    async () => await getUser(currentUser?.id),
    {
      enabled: !!currentUser,
    }
  );

  return (
    <Box
      display="flex"
      gap={1}
      sx={{
        backgroundColor: "#1c1e21",
        width: "100%",
        height: 46,
        borderBottom: "1px solid #555759",
      }}
    >
      {!currentUser ? (
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-end",
          }}
        >
          <Button
            variant="contained"
            onClick={() => handleNavigation("/sign-up")}
          >
            Register
          </Button>
          <Button
            variant="contained"
            onClick={() => handleNavigation("/sign-in")}
          >
            Login
          </Button>
        </Box>
      ) : (
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            width: "100%",
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Button onClick={() => handleNavigation("/")}>
              <FacebookIcon fontSize="large" />
            </Button>
            <SearchAppBar />
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Button>
              <HomeIcon />
            </Button>
            <Button>
              <PeopleIcon />
            </Button>
            <Button>
              <OndemandVideoIcon />
            </Button>
            <Button>
              <StorefrontIcon />
            </Button>
            <Button>
              <Diversity3Icon />
            </Button>
          </Box>

          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-end",
              gap: 1,
            }}
          >
            <Fab
              size="small"
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "#555759",
              }}
            >
              <MenuIcon sx={{ color: "white" }} />
            </Fab>
            <Fab
              size="small"
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "#555759",
              }}
            >
              <MessageIcon sx={{ color: "white" }} />
            </Fab>
            <Fab
              onClick={handleNotificationsIconClick}
              size="small"
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "#555759",
              }}
            >
              <NotificationsIcon sx={{ color: "white" }} />
            </Fab>
            <Fab
              size="small"
              onClick={handleAvatarClick}
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "#555759",
              }}
            >
              <Avatar
                src={userInformation?.avatarUrl}
                sx={{
                  color: "white",
                  width: 36,
                  height: 36,
                }}
              />
            </Fab>
          </Box>
          {isAvatarClicked && (
            <BasicMenu
              isAvatarClicked={isAvatarClicked}
              setIsAvatarClicked={setIsAvatarClicked}
            />
          )}
          {isNotificationsClicked && (
            <NotificationsContainer
              isNotificationsClicked={isNotificationsClicked}
              setIsNotificationsClicked={setIsNotificationsClicked}
            />
          )}
        </Box>
      )}
    </Box>
  );
};

export default Header;
