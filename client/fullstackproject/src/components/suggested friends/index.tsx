import Avatar from "@mui/material/Avatar";
import ListItemText from "@mui/material/ListItemText";
import { Box, Divider, Typography, Fab } from "@mui/material";
import {
  createNotification,
  getUser,
  getUserSuggestedFriends,
} from "../../api/api";
import { useQuery } from "react-query";
import { useSelector } from "react-redux";
import { selectUser } from "../../features/userSlice";
import { UserProps } from "../../types/types";
import AddIcon from "@mui/icons-material/Add";
import CircularIndeterminate from "../spinner/spinner";

export const SuggestedFriends: React.FC = () => {
  const currentUser = useSelector(selectUser);

  const { data: currentUserInfo } = useQuery<UserProps>(
    "userInformation",
    async () => await getUser(currentUser?.id),
    {
      enabled: !!currentUser,
    }
  );

  const {
    data: users,
    isSuccess,
    isFetching,
  } = useQuery<UserProps[]>(
    "users",
    async () => await getUserSuggestedFriends(currentUser.id),
    {
      enabled: !!currentUserInfo,
    }
  );

  const handleAddFriend = async (
    userId: number,
    recipientId: number
  ): Promise<void> => {
    await createNotification(userId, recipientId);
  };

  return (
    <Box
      sx={{
        marginLeft: 39,
        background: "#242526",
        width: 340,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        borderRadius: 2,
      }}
    >
      <Typography variant="h5" sx={{ color: "white" }}>
        Suggested Friends
      </Typography>
      <Divider
        sx={{
          background: "#e4e6eb",
          height: 1,
          width: "90%",
        }}
        variant="middle"
      ></Divider>
      {isFetching ? (
        <CircularIndeterminate />
      ) : (
        <Box
          sx={{
            maxHeight: "300px",
            overflowY: "scroll",
            "&::-webkit-scrollbar": {
              width: "0.5em",
            },
          }}
        >
          {isSuccess &&
            users.map((user: UserProps, index: number) => (
              <Box key={index}>
                <Box
                  sx={{
                    width: "100%",
                    maxWidth: 250,
                    borderRadius: "16px",
                    boxShadow: "0px 4px 8px rgba(0, 0, 0, 0.1)",
                    backgroundColor: "background.paper",
                    display: "flex",
                    alignItems: "center",
                    padding: "8px",
                    mt: 0.5,
                    background: "#555759",
                    gap: 1,
                  }}
                >
                  <Avatar src={user.imageUrl} />
                  <ListItemText
                    primary={`${user.firstname} ${user.lastname}`}
                    sx={{ flex: 1, paddingRight: "16px" }}
                  />
                  <Fab
                    size="small"
                    onClick={() => handleAddFriend(currentUser.id, user.id)}
                  >
                    <AddIcon></AddIcon>
                  </Fab>
                </Box>
              </Box>
            ))}
        </Box>
      )}
    </Box>
  );
};
