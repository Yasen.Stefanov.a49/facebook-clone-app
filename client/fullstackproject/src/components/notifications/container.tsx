import * as React from "react";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { Avatar, Box, Button } from "@mui/material";
import Typography from "@mui/material/Typography";
import { useQuery } from "react-query";
import { useSelector } from "react-redux";
import { selectUser } from "../../features/userSlice";
import {
  getUserReceivedNotifications,
  updateUserProperty,
} from "../../api/api";
import { UserProps } from "../../types/types";

export const NotificationsContainer: React.FC = ({
  isNotificationsClicked,
  setIsNotificationsClicked,
}) => {
  const currentUser = useSelector(selectUser);

  const handleClose = (): void => {
    setIsNotificationsClicked(!isNotificationsClicked);
  };

  const { data: notifications } = useQuery<UserProps[]>(
    "userNotifications",
    async () => (await getUserReceivedNotifications(currentUser?.id)) || [],
    {
      enabled: !!currentUser,
      onSuccess: (data) => {
        console.log(data);
      },
    }
  );
  const onAccept = async (userId: number): Promise<void> => {
    await updateUserProperty(currentUser.id, "friends", [userId.toString()]);
    await updateUserProperty(userId, "friends", [currentUser.id.toString()]);
  };
  const onDecline = async (): Promise<void> => {};

  return (
    <Menu
      anchorEl={null}
      open={isNotificationsClicked}
      onClose={handleClose}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      PaperProps={{
        sx: {
          background: "#3a3b3c",
        },
      }}
      sx={{ top: 30 }}
    >
      <Box sx={{ display: "flex", flexDirection: "column", width: 300 }}>
        <Box
          sx={{
            border: 0.1,
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            borderColor: "#3a3b3c",
            borderRadius: 2,
            marginLeft: 1,
            marginRight: 1,
          }}
        >
          <Typography variant="h6" sx={{ color: "#e4e6eb" }}>
            Notifications
          </Typography>
          {notifications &&
            notifications.length > 0 &&
            notifications.map((user: UserProps, index: number) => (
              <Box
                key={index}
                sx={{ display: "flex", flexDirection: "column" }}
              >
                <Box key={index} sx={{ display: "flex", alignItems: "center" }}>
                  <Avatar src={user.imageUrl}></Avatar>
                  <MenuItem key={index} sx={{ color: "#e4e6eb" }}>
                    {user.firstname} {user.lastname} <br />
                    has sent you a friend request.
                  </MenuItem>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    gap: 2,
                    justifyContent: "center",
                  }}
                >
                  <Button variant="contained" onClick={() => onAccept(user.id)}>
                    Accept
                  </Button>
                  <Button
                    variant="contained"
                    onClick={() => console.log(notifications)}
                  >
                    Decline
                  </Button>
                </Box>
              </Box>
            ))}
        </Box>
      </Box>
    </Menu>
  );
};
