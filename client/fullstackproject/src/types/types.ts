export type UserFormProps = {
  firstname: string;
  lastname: string;
  email: string;
  age: number;
  gender: string;
  password: string;
};

export type LoginFormProps = {
  email: string;
  password: string;
};

export type FileProps = {
  description: string;
  filename: string;
  id: number;
  imageData: {
    data: number[];
  };
  userId: number;
};

export type NavigationProps = {
  handleNavigation: (path: string, state?: object | null | undefined) => void;
};

export type UserProps = {
  id: number;
  firstname: string;

  lastname: string;

  email: string;

  age: number;

  gender: string;

  password: string;

  friends: any[];
  friendRequests: any[];
  notifications: any[];
  messages: any[];

  bio: string;

  livesIn: string;

  bornIn: string;

  worksAt: string;

  secondarySchool: string;

  university: string;

  relationship: string;
  hobbies: string[];
  avatarId: number;
  images: Buffer[];
  imageUrl: string;
  data?: string;
  postId?: number;
  likedBy: number[];
  sharedBy: number[];
  notificationId?: number;
  avatarUrl: string;
};

export type NotificationProps = {
  from: number;
  to: number;
  avatarId: number;
};

export type PostProps = {
  userId: number;
  data: string;
  likedBy: number[];
};

export type PostWithUser = {
  id: number;
  userId: number;
  data: string;
  likedBy: number[];
  commentedBy: string[];
  sharedBy: string[];
  user: {
    id: number;
    firstname: string;
    lastname: string;
  };
};
