import React, { ChangeEvent } from "react";
import { Avatar, Box, Divider } from "@mui/material";
import { useParams } from "react-router-dom";
import {
  getUser,
  getUserAvatar,
  updateUserProperty,
  uploadImage,
} from "../../api/api";
import { Button } from "@mui/material";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import BasicTabs from "./profiletabs";
import { useState, useRef } from "react";
import ImageIcon from "@mui/icons-material/Image";
import UploadIcon from "@mui/icons-material/Upload";
import FaceIcon from "@mui/icons-material/Face";
import { MenuItem } from "@mui/material";
import AddAPhotoIcon from "@mui/icons-material/AddAPhoto";
import EmojiEmotionsIcon from "@mui/icons-material/EmojiEmotions";
import { Intro } from "./intro";
import Images from "./images";
import { FileProps } from "../../types/types";
import { useQuery, useMutation, useQueryClient } from "react-query";

export const Profile: React.FC = () => {
  const queryClient = useQueryClient();
  const { id } = useParams();
  const [isCoverBoxClicked, setIsCoverBoxClicked] = useState<boolean>(false);
  const { data: userInformation, isLoading } = useQuery(
    "userInfo",
    async () => await getUser(id),
    {
      enabled: !!id,
    }
  );
  const { data: userAvatar } = useQuery(
    "userAvatar",
    async () => await getUserAvatar(+userInformation?.avatarId),
    {
      enabled: !!userInformation?.avatarId,
      onSuccess: (data) => convertByteArrayToImage(data),
    }
  );

  const [avatarURL, setAvatarURL] = useState<string>("");
  const coverBoxOptions = [
    {
      name: "Choose cover photo",
      icon: ImageIcon,
    },
    {
      name: "Upload photo",
      icon: UploadIcon,
    },
    {
      name: "Create avatar cover photo",
      icon: FaceIcon,
    },
  ];
  const buttonsCoverBox = [
    {
      name: "Create with avatar",
      icon: EmojiEmotionsIcon,
    },
    {
      name: "Add Cover Photo",
      icon: AddAPhotoIcon,
    },
  ];
  const handleCoverBoxToggle: () => void = () => {
    setIsCoverBoxClicked(!isCoverBoxClicked);
  };

  const inputRef = useRef<HTMLInputElement>(null);

  const handleAvatarClick: () => void = () => {
    if (inputRef.current) {
      inputRef.current.click();
    }
  };
  const updateAvatarMutation = useMutation(
    (avatarId: number) =>
      updateUserProperty(+userInformation!.id, "avatarId", +avatarId!),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("userImagesArray");
      },
    }
  );

  const handleFileChange = async (e: ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (file) {
      const avatarId = await uploadImage(+userInformation!.id, file);
      await updateAvatarMutation.mutateAsync(+avatarId!);
      const avatar = await getUserAvatar(avatarId!);
      convertByteArrayToImage(avatar!);
    }
  };

  const convertByteArrayToImage = (file: FileProps) => {
    const byteArray = new Uint8Array(file.imageData.data);
    const blob = new Blob([byteArray], { type: "image/jpeg" });
    const dataUrl = URL.createObjectURL(blob);
    setAvatarURL(dataUrl);
    return dataUrl;
  };

  return (
    <Box>
      <Box sx={{ height: "550px", width: "100%", background: "#1c1e21" }}>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "flex-end",
            alignItems: "flex-end",
            height: 375,
            marginLeft: "300px",
            marginRight: "300px",
            background: "#18191a",
            borderRadius: 2,
            boxShadow: 1,
          }}
          onClick={handleCoverBoxToggle}
        >
          {buttonsCoverBox.map((button, i: number) => (
            <MenuItem
              onClick={() => console.log("add cover photo")}
              key={i}
              sx={{
                margin: 0.05,
                width: 200,
                color: "#e4e6eb",
                gap: 1,
                "&:hover": {
                  backgroundColor: "#555759",
                  borderRadius: "8px",
                },
              }}
            >
              <button.icon />
              {button.name}
            </MenuItem>
          ))}

          <Box
            sx={{
              width: 300,
              background: "#3a3b3c",
              borderRadius: 2,
              position: "absolute",
              top: 46,
            }}
          >
            {isCoverBoxClicked &&
              coverBoxOptions.map((option, i: number) => (
                <MenuItem
                  key={i}
                  sx={{
                    display: "flex",
                    margin: 0.05,
                    width: 280,
                    color: "#e4e6eb",
                    marginLeft: 1,
                    marginRight: 1,
                    gap: 0.5,
                    "&:hover": {
                      backgroundColor: "#555759",
                      borderRadius: "8px",
                    },
                  }}
                >
                  <option.icon />
                  {option.name}
                </MenuItem>
              ))}
          </Box>
        </Box>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            marginLeft: "300px",
            marginRight: "300px",
            justifyContent: "space-between",
            gap: 2,
          }}
        >
          {!isLoading && (
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                gap: 2,
                color: "white",
                fontSize: 36,
                fontWeight: "bold",
              }}
            >
              <Avatar
                src={avatarURL}
                onClick={handleAvatarClick}
                sx={{ height: 140, width: 140, marginTop: -5, marginLeft: 4 }}
              />
              {userInformation?.firstname} {userInformation?.lastname}
              <input
                type="file"
                accept="image/*"
                ref={inputRef}
                style={{ display: "none" }}
                onChange={handleFileChange}
              />
            </Box>
          )}
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-end",
              gap: 1,
            }}
          >
            <Button variant="contained">
              <AddIcon />
              Add to story
            </Button>
            <Button
              variant="contained"
              sx={{
                backgroundColor: "#3a3b3c",
                "&:hover": {
                  backgroundColor: "#555759",
                },
              }}
            >
              <EditIcon />
              Edit profile
            </Button>
            <Button
              variant="contained"
              sx={{
                backgroundColor: "#3a3b3c",
                "&:hover": {
                  backgroundColor: "#555759",
                },
              }}
            >
              <ArrowDropDownIcon />
            </Button>
          </Box>
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            marginLeft: "300px",
            marginRight: "315px",
          }}
        >
          <Divider
            sx={{ background: "#e4e6eb", width: "100%", marginTop: 2 }}
            variant="middle"
          ></Divider>
          <BasicTabs />
        </Box>
      </Box>
      <Box
        sx={{
          width: "auto",
          display: "flex",
          flexDirection: "column",
          background: "#18191a",
        }}
      >
        <Intro />
        <Images />
      </Box>
    </Box>
  );
};
