import React from "react";
import Box from "@mui/material/Box";
import { Button, Typography, TextareaAutosize, Chip } from "@mui/material";
import { useState } from "react";
import HomeIcon from "@mui/icons-material/Home";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import { getUser, updateUserProperty } from "../../api/api";
import { selectUser } from "../../features/userSlice.ts";
import { useSelector } from "react-redux";
import { useQuery, useMutation, useQueryClient } from "react-query";
import { ModalUnstyled } from "./userdetailsmodal.tsx";
import { AddHobbies } from "./addhobbies.tsx";

export const Intro: React.FC = () => {
  const queryClient = useQueryClient();
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [editedBio, setEditedBio] = useState<string>("");
  const currentUser = useSelector(selectUser);
  const { data: user } = useQuery(
    "userInformation",
    async () => await getUser(currentUser.id),
    { enabled: !!currentUser }
  );

  const updateBioMutation = useMutation(
    (newBio: string) => updateUserProperty(currentUser.id, "bio", newBio),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("userInformation");
      },
    }
  );

  const handleSaveClick: () => Promise<void> = async () => {
    setIsEditing(false);
    await updateBioMutation.mutateAsync(editedBio);
  };

  const handleEditClick: () => void = () => {
    setIsEditing(true);
    setEditedBio(user?.bio || "");
  };

  return (
    <Box
      sx={{
        marginTop: 2,
        display: "flex",
        flexDirection: "column",
        alignItems: "flex-start",
        width: 400,
        height: "auto",
        color: "white",
        marginLeft: "300px",
        marginRight: "300px",
        background: "#242526",
        borderRadius: 2,
      }}
    >
      <Typography
        sx={{
          display: "flex",
          alignItems: "flex-start",
          padding: 1.5,
          fontWeight: "bold",
        }}
      >
        Intro
      </Typography>
      {isEditing ? (
        <Box>
          <TextareaAutosize
            value={editedBio}
            maxRows={3}
            maxLength={75}
            style={{
              borderRadius: 5,
              background: "#555759",
              color: "white",
              width: 340,
              minWidth: 340,
              maxWidth: 350,
              minHeight: 60,
              maxHeight: 80,
            }}
            onChange={(e) => setEditedBio(e.target.value)}
          ></TextareaAutosize>
          <Button
            variant="contained"
            sx={{ margin: 1.5, width: 360, background: "#3a3b3c" }}
            disabled={editedBio.trim().length === 0}
            onClick={handleSaveClick}
          >
            Save
          </Button>
        </Box>
      ) : (
        <Box>
          {user?.bio ? (
            <Typography sx={{ display: "flex", paddingLeft: 1.5 }}>
              {user.bio}
            </Typography>
          ) : null}
          <Button
            variant="contained"
            sx={{ margin: 1.5, width: 360, background: "#3a3b3c" }}
            onClick={handleEditClick}
          >
            {user?.bio ? "Edit Bio" : "Add Bio"}
          </Button>
        </Box>
      )}
      {user?.livesIn && (
        <Typography
          sx={{
            display: "flex",
            alignItems: "center",
            gap: 1,
            paddingLeft: 1,
            marginBottom: 1,
          }}
        >
          <HomeIcon />
          Lives in {user.livesIn}
        </Typography>
      )}
      {user?.bornIn && (
        <Box>
          <Typography
            sx={{
              display: "flex",
              alignItems: "center",
              gap: 1,
              paddingLeft: 1,
            }}
          >
            <LocationOnIcon />
            From {user.bornIn}
          </Typography>
        </Box>
      )}
      <ModalUnstyled />
      {user?.hobbies?.length > 0 && (
        <Typography sx={{ marginLeft: 1 }}>Your hobbies:</Typography>
      )}
      <Box sx={{ display: "flex", flexWrap: "wrap" }}>
        {user?.hobbies?.length > 0 &&
          user.hobbies.map((hobby: string) => (
            <Chip
              key={hobby}
              sx={{
                color: "white",
                margin: 0.2,
                cursor: "pointer",
                "&:hover": {
                  background: "#b0b3b8",
                },
              }}
              label={hobby}
            ></Chip>
          ))}
      </Box>
      <AddHobbies />
    </Box>
  );
};
