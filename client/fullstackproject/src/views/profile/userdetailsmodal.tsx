import * as React from "react";
import clsx from "clsx";
import { styled, Box } from "@mui/system";
import { Modal as BaseModal } from "@mui/base/Modal";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import Link from "@mui/material/Link";
import { Divider, TextField } from "@mui/material";
import { updateUserProperty } from "../../api/api";
import { selectUser } from "../../features/userSlice";
import { useSelector } from "react-redux";
import { useState } from "react";

export const ModalUnstyled: React.FC = () => {
  const user = useSelector(selectUser);
  const [open, setOpen] = useState<boolean>(false);
  const [isAddWorkplaceOpen, setIsAddWorkplaceOpen] = useState<boolean>(false);
  const [isAddSecondarySchoolOpen, setIsAddSecondarySchoolOpen] =
    useState<boolean>(false);
  const [isAddUniversityOpen, setIsAddUniversityOpen] =
    useState<boolean>(false);
  const [isRelationshipOpen, setIsRelationshipOpen] = useState<boolean>(false);
  const [isHometownOpen, setIsHometownOpen] = useState<boolean>(false);
  const [isLocationOpen, setIsLocationOpen] = useState<boolean>(false);
  const [workSpaceInformation, setWorkspaceInformation] = useState<string>("");
  const [secondarySchoolInformation, setSecondarySchoolInformation] =
    useState<string>("");
  const [universityInformation, setUniversityInformation] =
    useState<string>("");
  const [hometownInformation, setHometownInformation] = useState<string>("");
  const [locationInformation, setLocationInformation] = useState<string>("");
  const [relationshipInformation, setRelationshipInformation] =
    useState<string>("");
  const handleOpen: () => void = () => setOpen(true);
  const handleClose: () => void = () => setOpen(false);
  const handleAddWorkspace: () => void = () => {
    setIsAddWorkplaceOpen(!isAddWorkplaceOpen);
  };
  const handleAddSecondarySchool: () => void = () => {
    setIsAddSecondarySchoolOpen(!isAddSecondarySchoolOpen);
  };
  const handleAddUniversity: () => void = () => {
    setIsAddUniversityOpen(!isAddUniversityOpen);
  };
  const handleAddHometown: () => void = () => {
    setIsHometownOpen(!isHometownOpen);
  };
  const handleLocationOpen: () => void = () => {
    setIsLocationOpen(!isLocationOpen);
  };
  const handleRelationshipOpen: () => void = () => {
    setIsRelationshipOpen(!isRelationshipOpen);
  };
  const handleSave = async () => {
    if (workSpaceInformation) {
      await updateUserProperty(user.id, "worksAt", workSpaceInformation);
    }
    if (secondarySchoolInformation) {
      await updateUserProperty(
        user.id,
        "secondarySchool",
        secondarySchoolInformation
      );
    }
    if (universityInformation) {
      await updateUserProperty(user.id, "university", universityInformation);
    }
    if (hometownInformation) {
      await updateUserProperty(user.id, "bornIn", hometownInformation);
    }
    if (locationInformation) {
      await updateUserProperty(user.id, "livesIn", locationInformation);
    }
    if (relationshipInformation) {
      await updateUserProperty(
        user.id,
        "relationship",
        relationshipInformation
      );
    }
    handleClose();
    handleAddSecondarySchool();
    handleAddUniversity();
    handleAddHometown();
    handleLocationOpen();
    handleRelationshipOpen();
    handleAddWorkspace();
  };

  return (
    <Box>
      <Button
        sx={{ margin: 1.5, width: 360, background: "#3a3b3c", color: "white" }}
        onClick={handleOpen}
      >
        Edit details
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        slots={{ backdrop: StyledBackdrop }}
      >
        <ModalContent
          sx={{ width: 400, background: "#3a3b3c", color: "white" }}
        >
          <Typography
            sx={{ display: "flex", alignSelf: "center" }}
            variant="h5"
          >
            Edit details
          </Typography>
          <Divider sx={{ background: "white" }} />
          <Typography variant="h6" className="modal-title">
            Customize your Intro
          </Typography>
          <Typography className="modal-title">
            Details you select will be public.
          </Typography>
          <Typography variant="h6" className="modal-title">
            Work
          </Typography>
          <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
            <AddCircleOutlineIcon />
            <Link onClick={handleAddWorkspace} sx={{ cursor: "pointer" }}>
              <Typography variant="h6">Add a workplace</Typography>
            </Link>
          </Box>
          {isAddWorkplaceOpen && (
            <Box>
              <TextField
                style={{ color: "white" }}
                label="Add workspace"
                onChange={(e) => setWorkspaceInformation(e.target.value)}
                value={workSpaceInformation}
              />
            </Box>
          )}
          <Typography variant="h6" className="modal-title">
            Education
          </Typography>
          <Box
            sx={{
              display: "flex",
              gap: 1,
              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
              <AddCircleOutlineIcon />
              <Link
                onClick={handleAddSecondarySchool}
                sx={{ cursor: "pointer" }}
              >
                <Typography variant="h6">Add a secondary school</Typography>
              </Link>
            </Box>
            {isAddSecondarySchoolOpen && (
              <Box>
                <TextField
                  style={{ color: "white" }}
                  label="Add secondary school"
                  onChange={(e) =>
                    setSecondarySchoolInformation(e.target.value)
                  }
                  value={secondarySchoolInformation}
                />
              </Box>
            )}
            <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
              <AddCircleOutlineIcon />
              <Link onClick={handleAddUniversity} sx={{ cursor: "pointer" }}>
                <Typography variant="h6">Add university</Typography>
              </Link>
            </Box>
            {isAddUniversityOpen && (
              <Box>
                <TextField
                  style={{ color: "white" }}
                  label="Add university"
                  onChange={(e) => setUniversityInformation(e.target.value)}
                  value={universityInformation}
                />
              </Box>
            )}
          </Box>
          <Typography variant="h6" className="modal-title">
            Location
          </Typography>
          <Box
            sx={{
              display: "flex",
              gap: 1,
              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
              <AddCircleOutlineIcon />
              <Link onClick={handleAddHometown} sx={{ cursor: "pointer" }}>
                <Typography variant="h6">Add hometown</Typography>
              </Link>
            </Box>
            {isHometownOpen && (
              <Box>
                <TextField
                  style={{ color: "white" }}
                  label="Add hometown"
                  onChange={(e) => setHometownInformation(e.target.value)}
                  value={hometownInformation}
                />
              </Box>
            )}
            <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
              <AddCircleOutlineIcon />
              <Link onClick={handleLocationOpen} sx={{ cursor: "pointer" }}>
                <Typography variant="h6">Add location</Typography>
              </Link>
            </Box>
            {isLocationOpen && (
              <Box>
                <TextField
                  style={{ color: "white" }}
                  label="Add location"
                  onChange={(e) => setLocationInformation(e.target.value)}
                  value={locationInformation}
                />
              </Box>
            )}
            <Typography variant="h6" className="modal-title">
              Relationship
            </Typography>
            <Box
              sx={{
                display: "flex",
                gap: 1,
                justifyContent: "center",
                flexDirection: "column",
              }}
            >
              <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
                <AddCircleOutlineIcon />
                <Link
                  onClick={handleRelationshipOpen}
                  sx={{ cursor: "pointer" }}
                >
                  <Typography variant="h6">Add relationship</Typography>
                </Link>
              </Box>
              {isRelationshipOpen && (
                <Box>
                  <TextField
                    style={{ color: "white" }}
                    label="Add relationship"
                    onChange={(e) => setRelationshipInformation(e.target.value)}
                    value={relationshipInformation}
                  />
                </Box>
              )}
            </Box>
            <Divider sx={{ background: "white" }} />
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-end",
                gap: 1,
              }}
            >
              <Button onClick={handleClose}>Cancel</Button>
              <Button onClick={handleSave}>Save</Button>
            </Box>
          </Box>
        </ModalContent>
      </Modal>
    </Box>
  );
};

const Backdrop = React.forwardRef<
  HTMLDivElement,
  { open?: boolean; className: string }
>((props, ref) => {
  const { open, className, ...other } = props;
  return (
    <div
      className={clsx({ "MuiBackdrop-open": open }, className)}
      ref={ref}
      {...other}
    />
  );
});

const blue = {
  200: "#99CCFF",
  300: "#66B2FF",
  400: "#3399FF",
  500: "#007FFF",
  600: "#0072E5",
  700: "#0066CC",
};

const grey = {
  50: "#F3F6F9",
  100: "#E5EAF2",
  200: "#DAE2ED",
  300: "#C7D0DD",
  400: "#B0B8C4",
  500: "#9DA8B7",
  600: "#6B7A90",
  700: "#434D5B",
  800: "#303740",
  900: "#1C2025",
};

const Modal = styled(BaseModal)`
  position: fixed;
  z-index: 1300;
  inset: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledBackdrop = styled(Backdrop)`
  z-index: -1;
  position: fixed;
  inset: 0;
  background-color: rgb(0 0 0 / 0.5);
  -webkit-tap-highlight-color: transparent;
`;

const ModalContent = styled(Box)(
  ({ theme }) => `
  display: flex;
  flex-direction: column;
  gap: 8px;
  overflow: hidden;
  background-color: ${theme.palette.mode === "dark" ? grey[900] : "#FFF"};
  border-radius: 8px;
  border: 1px solid ${theme.palette.mode === "dark" ? grey[700] : grey[200]};
  box-shadow: 0px 4px 12px ${
    theme.palette.mode === "dark" ? "rgba(0,0,0, 0.50)" : "rgba(0,0,0, 0.20)"
  };
  padding: 1rem;
  color: ${theme.palette.mode === "dark" ? grey[50] : grey[900]};
  font-family: IBM Plex Sans, sans-serif;
  font-weight: 500;
  text-align: start;
  position: relative;
  & .MuiTextField-root {
    width: 100%;
  }

  & .MuiButton-root {
    background: ${theme.palette.mode === "dark" ? grey[900] : "#fff"};
    border: 1px solid ${theme.palette.mode === "dark" ? grey[700] : grey[200]};
    color: ${theme.palette.mode === "dark" ? grey[200] : grey[900]};
    box-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);
    transition: all 150ms ease;
    cursor: pointer;

    &:hover {
      background: ${theme.palette.mode === "dark" ? grey[800] : grey[50]};
      border-color: ${theme.palette.mode === "dark" ? grey[600] : grey[300]};
    }

    &:active {
      background: ${theme.palette.mode === "dark" ? grey[700] : grey[100]};
    }

    &:focus-visible {
      box-shadow: 0 0 0 4px ${
        theme.palette.mode === "dark" ? blue[300] : blue[200]
      };
      outline: none;
    }
  }
`
);
