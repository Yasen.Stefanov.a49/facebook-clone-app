import * as React from "react";
import clsx from "clsx";
import { styled, Box } from "@mui/system";
import { Modal as BaseModal } from "@mui/base/Modal";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { Divider } from "@mui/material";
import { hobbies } from "./hobbies";
import Chip from "@mui/material/Chip";
import { useState } from "react";
import { updateUserProperty } from "../../api/api";
import { selectUser } from "../../features/userSlice.ts";
import { useSelector } from "react-redux";
import { getUser } from "../../api/api";
import { useQuery, useMutation, useQueryClient } from "react-query";
export const AddHobbies: React.FC = () => {
  const queryClient = useQueryClient();
  const currentUser = useSelector(selectUser);
  const [open, setOpen] = React.useState<boolean>(false);
  const handleOpen: () => void = () => setOpen(true);
  const handleClose: () => void = () => setOpen(false);
  const [selectedHobbies, setSelectedHobbies] = useState<string[]>([]);
  const { data: user } = useQuery(
    "userInfo",
    async () => await getUser(currentUser.id),
    {
      enabled: !!currentUser,
    }
  );

  const handleClick = (hobby: string) => {
    if (user?.hobbies?.includes(hobby) || selectedHobbies.includes(hobby)) {
      setSelectedHobbies(selectedHobbies.filter((item) => item !== hobby));
      console.log(selectedHobbies);
    } else {
      setSelectedHobbies([...selectedHobbies, hobby]);
    }
  };
  const updateHobbiesMutation = useMutation(
    (newHobby: string) =>
      updateUserProperty(currentUser.id, "hobbies", newHobby),
    {
      onSuccess: () => {
        queryClient.invalidateQueries("userInformation");
      },
    }
  );

  const handleSave: () => Promise<void> = async () => {
    await updateHobbiesMutation.mutateAsync(selectedHobbies);
    handleClose();
  };
  const userHobbies = user?.hobbies || [];
  return (
    <Box>
      <Button
        sx={{ margin: 1.5, width: 360, background: "#3a3b3c", color: "white" }}
        onClick={handleOpen}
      >
        {userHobbies.length > 0 ? "Edit hobbies" : "Add hobbies"}
      </Button>
      <Modal
        aria-labelledby="unstyled-modal-title"
        aria-describedby="unstyled-modal-description"
        open={open}
        onClose={handleClose}
        slots={{ backdrop: StyledBackdrop }}
      >
        <ModalContent
          sx={{ width: 500, background: "#3a3b3c", color: "white" }}
        >
          <Typography
            sx={{ display: "flex", alignSelf: "center" }}
            variant="h5"
          >
            Add Hobbies
          </Typography>
          <Typography
            sx={{ fontSize: 13 }}
            variant="h6"
            className="modal-title"
          >
            What do you love to do? Choose from the popular hobbies below or add
            others.
          </Typography>
          <Divider sx={{ background: "white" }} />

          <Typography
            variant="h6"
            className="modal-title"
            sx={{ display: "flex", justifyContent: "center" }}
          >
            Recommended Hobbies
          </Typography>

          <Box
            sx={{
              display: "flex",
              gap: 0.5,
              flexWrap: "wrap",
            }}
          >
            {hobbies.slice(0, 11).map((hobby: string) => (
              <Chip
                label={hobby}
                key={hobby}
                onClick={() => handleClick(hobby)}
                sx={{
                  cursor: "pointer",
                  backgroundColor: selectedHobbies?.includes(hobby)
                    ? "#b0b3b8"
                    : "",
                  color: "white",
                  "&:hover": {
                    background: "#b0b3b8",
                  },
                }}
              />
            ))}
          </Box>

          <Divider sx={{ background: "white" }} />
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-end",
              gap: 1,
            }}
          >
            <Button onClick={handleClose}>Cancel</Button>
            <Button onClick={handleSave}>Save</Button>
          </Box>
        </ModalContent>
      </Modal>
    </Box>
  );
};

const Backdrop = React.forwardRef<
  HTMLDivElement,
  { open?: boolean; className: string }
>((props, ref) => {
  const { open, className, ...other } = props;
  return (
    <div
      className={clsx({ "MuiBackdrop-open": open }, className)}
      ref={ref}
      {...other}
    />
  );
});

const blue = {
  200: "#99CCFF",
  300: "#66B2FF",
  400: "#3399FF",
  500: "#007FFF",
  600: "#0072E5",
  700: "#0066CC",
};

const grey = {
  50: "#F3F6F9",
  100: "#E5EAF2",
  200: "#DAE2ED",
  300: "#C7D0DD",
  400: "#B0B8C4",
  500: "#9DA8B7",
  600: "#6B7A90",
  700: "#434D5B",
  800: "#303740",
  900: "#1C2025",
};

const Modal = styled(BaseModal)`
  position: fixed;
  z-index: 1300;
  inset: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledBackdrop = styled(Backdrop)`
  z-index: -1;
  position: fixed;
  inset: 0;
  background-color: rgb(0 0 0 / 0.5);
  -webkit-tap-highlight-color: transparent;
`;

const ModalContent = styled(Box)(
  ({ theme }) => `
  display: flex;
  flex-direction: column;
  gap: 8px;
  overflow: hidden;
  background-color: ${theme.palette.mode === "dark" ? grey[900] : "#FFF"};
  border-radius: 8px;
  border: 1px solid ${theme.palette.mode === "dark" ? grey[700] : grey[200]};
  box-shadow: 0px 4px 12px ${
    theme.palette.mode === "dark" ? "rgba(0,0,0, 0.50)" : "rgba(0,0,0, 0.20)"
  };
  padding: 1rem;
  color: ${theme.palette.mode === "dark" ? grey[50] : grey[900]};
  font-family: IBM Plex Sans, sans-serif;
  font-weight: 500;
  text-align: start;
  position: relative;
  & .MuiTextField-root {
    width: 100%;
  }

  & .MuiButton-root {
    background: ${theme.palette.mode === "dark" ? grey[900] : "#fff"};
    border: 1px solid ${theme.palette.mode === "dark" ? grey[700] : grey[200]};
    color: ${theme.palette.mode === "dark" ? grey[200] : grey[900]};
    box-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);
    transition: all 150ms ease;
    cursor: pointer;

    &:hover {
      background: ${theme.palette.mode === "dark" ? grey[800] : grey[50]};
      border-color: ${theme.palette.mode === "dark" ? grey[600] : grey[300]};
    }

    &:active {
      background: ${theme.palette.mode === "dark" ? grey[700] : grey[100]};
    }

    &:focus-visible {
      box-shadow: 0 0 0 4px ${
        theme.palette.mode === "dark" ? blue[300] : blue[200]
      };
      outline: none;
    }
  }
`
);
