import React from "react";
import { useQuery } from "react-query";
import { getUserImages } from "../../api/api";
import { useParams } from "react-router-dom";
import { Box, Typography } from "@mui/material";
import { FileProps } from "../../types/types";
const Images: React.FC = () => {
  const { id } = useParams();

  const { data: userImages, isFetched } = useQuery(
    "userImagesArray",
    async () => (await getUserImages(+id!)) || [],
    {
      enabled: !!id,
    }
  );

  return (
    <Box
      sx={{
        marginTop: 2,
        display: "flex",
        flexDirection: "column",
        alignItems: "flex-start",
        width: 400,
        height: 400,
        color: "white",
        marginLeft: "300px",
        marginRight: "300px",
        background: "#242526",
        borderRadius: 2,
      }}
    >
      <Typography
        sx={{
          color: "white",
          marginLeft: 1.5,
          fontWeight: "bold",
          marginTop: 1.5,
        }}
      >
        Images
      </Typography>
      <Box sx={{ maxWidth: 400 }}>
        {isFetched && userImages && userImages.length > 0 ? (
          userImages.slice(0, 9).map((imageObj: FileProps, index: number) => {
            const byteArray = new Uint8Array(imageObj.imageData.data);
            const blob = new Blob([byteArray], { type: "image/jpeg" });
            const dataUrl = URL.createObjectURL(blob);

            return (
              <img
                key={index}
                src={dataUrl}
                alt={`Image ${index + 1}`}
                style={{
                  width: "100px",
                  height: "100px",
                  margin: "10px",
                  borderRadius: 2,
                }}
              />
            );
          })
        ) : (
          <p>No images available.</p>
        )}
      </Box>
    </Box>
  );
};

export default Images;
