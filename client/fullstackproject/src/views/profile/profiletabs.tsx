import * as React from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { Button } from "@mui/material";
import { useState } from "react";
import MenuItem from "@mui/material/MenuItem";
import ArrowDropDown from "@mui/icons-material/ArrowDropDown";
import Menu from "@mui/icons-material/Menu";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import SearchIcon from "@mui/icons-material/Search";
import ReportProblemIcon from "@mui/icons-material/ReportProblem";
import ArchiveIcon from "@mui/icons-material/Archive";
import HistoryToggleOffIcon from "@mui/icons-material/HistoryToggleOff";
import TableViewIcon from "@mui/icons-material/TableView";
import PersonIcon from "@mui/icons-material/Person";
import BusinessCenterIcon from "@mui/icons-material/BusinessCenter";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function CustomTabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function BasicTabs() {
  const [value, setValue] = React.useState(0);
  const [isMoreOpened, setIsMoreOpened] = useState(false);
  const [isMenuOpened, setIsMenuOpened] = useState(false);

  const handleMoreTab = () => {
    setIsMoreOpened(!isMoreOpened);
    setIsMenuOpened(false);
  };
  const handleMenuTab = () => {
    setIsMenuOpened(!isMenuOpened);
    setIsMoreOpened(false);
  };

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };
  const tabsOptions = [
    {
      name: "Posts",
    },
    {
      name: "About",
    },
    {
      name: "Friends",
    },
    {
      name: "Photos",
    },
    {
      name: "Videos",
    },
    {
      name: "Check-ins",
    },
  ];
  const moreOptions = [
    {
      name: "Sports",
    },
    {
      name: "Music",
    },
    {
      name: "Films",
    },
    {
      name: "TV Programmes",
    },
    {
      name: "Books",
    },
    {
      name: "Apps and Games",
    },
    {
      name: "Likes",
    },
    {
      name: "Events",
    },
    {
      name: "Reviews given",
    },
    {
      name: "Groups",
    },
    {
      name: "Manage Sections",
    },
  ];
  const menuOptions = [
    {
      name: "View as",
      icon: RemoveRedEyeIcon,
    },
    {
      name: "Search",
      icon: SearchIcon,
    },
    {
      name: "Profile status",
      icon: ReportProblemIcon,
    },
    {
      name: "Archive",
      icon: ArchiveIcon,
    },
    {
      name: "Story archive",
      icon: HistoryToggleOffIcon,
    },
    {
      name: "Activity log",
      icon: TableViewIcon,
    },
    {
      name: "Profile and tagging settings",
      icon: PersonIcon,
    },
    {
      name: "Turn on professional mode",
      icon: BusinessCenterIcon,
    },
  ];

  return (
    <Box sx={{ width: "100%" }}>
      <Box
        sx={{ width: "100%", display: "flex", justifyContent: "space-between" }}
      >
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          {tabsOptions.map((tab, index: number) => (
            <Tab
              key={index}
              label={tab.name}
              {...a11yProps(index)}
              sx={{ color: "white" }}
            />
          ))}
          <Button variant="contained" onClick={handleMoreTab}>
            More
            <ArrowDropDown />
          </Button>
        </Tabs>
        <Box
          sx={{
            display: "flex",
            justifyContent: "flex-end",
            alignItems: "flex-end",
          }}
        >
          <Button
            variant="contained"
            onClick={handleMenuTab}
            sx={{
              display: "flex",
              background: "#3a3b3c",
            }}
          >
            <Menu />
          </Button>
        </Box>
      </Box>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Box
          sx={{
            width: 300,
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            background: "#3a3b3c",
            borderRadius: 2,
          }}
        >
          {isMoreOpened &&
            moreOptions.map((option, i) => (
              <MenuItem
                key={i}
                sx={{
                  margin: 0.05,
                  width: 280,
                  display: "flex",
                  color: "#e4e6eb",
                  marginLeft: 1,
                  marginRight: 1,
                  "&:hover": {
                    backgroundColor: "#555759",
                    borderRadius: "8px",
                  },
                }}
              >
                {option.name}
              </MenuItem>
            ))}
        </Box>
        <Box
          sx={{
            width: 300,
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            background: "#3a3b3c",
            borderRadius: 2,
            marginLeft: 110,
          }}
        >
          {isMenuOpened &&
            menuOptions.map((option, i) => (
              <MenuItem
                key={i}
                sx={{
                  margin: 0.05,
                  width: 280,
                  display: "flex",
                  color: "#e4e6eb",
                  marginLeft: 1,
                  marginRight: 1,
                  gap: 1,
                  "&:hover": {
                    backgroundColor: "#555759",
                    borderRadius: "8px",
                  },
                }}
              >
                <option.icon />
                {option.name}
              </MenuItem>
            ))}
        </Box>
        {tabsOptions.map((_, index: number) => (
          <CustomTabPanel
            key={index}
            value={value}
            index={index}
          ></CustomTabPanel>
        ))}
      </Box>
    </Box>
  );
}
